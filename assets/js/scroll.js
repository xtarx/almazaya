//http://www.sanwebe.com/2013/05/auto-load-records-on-page-scroll
var classified_template='<div class="col-lg-3 col-md-4 col-sm-6"><div class="ad-element"><a href="{link}" ><div class="ad-photo" style="background-image: url({image});"><div class="overlay"><span class="glyphicon glyphicon-eye-open"></span></div></div></a><div class="ad-details row"><div class="col-xs-8"><a href="{link}" class="ad-title">{title}</a><div class="ad-owner">{description}</div></div></div><div class="separator"></div><div class="ad-details row"><span class="glyphicon glyphicon-edit"></span>{likes}</div></div></div>';

$(document).ready(function() {
    var track_load = 2; //total loaded record group(s)
    var loading  = false; //to prevents multipal ajax loads
    var done_pagination=false;
    
    $(window).scroll(function() { //detect page scroll
        
        if($(window).scrollTop() + $(window).height() == $(document).height())  //user scrolled to bottom of the page?
        {
            
            if(loading==false) //there's more data to load
            {
                loading = true; //prevent further ajax loading
                $('.spinner').show(); //show loading image
                
                if(!done_pagination &&!loading){

                //load data from the server using a HTTP POST request
                loading = true; //prevent further ajax loading

                $.post(window.location,{p:track_load},function(result){

                        obj = JSON.parse(result);
                        // console.log(obj);
                        var ads=obj.ads;  

                        if(ads.length==0){
                            //ads done then
                            done_pagination=true;
                            return;
                        }
                        var classified_new="";
                    for (var i = 0; i < ads.length; i++) {
                        var temp = classified_template.replace(new RegExp('{link}', 'g'),base_url+"classified_ads/view/"+ads[i].id);
                        temp = temp.replace(new RegExp('{image}', 'g'),base_url+"uploads/classified_ads/images/"+ads[i].image);
                        temp = temp.replace(new RegExp('{title}', 'g'),ads[i].title);
                        temp = temp.replace(new RegExp('{description}', 'g'),ads[i].description);
                        temp = temp.replace(new RegExp('{likes}', 'g'),ads[i].likes);
                        classified_new+=temp;
                        console.log(temp);
                    }


                   // console.log("here");
                    $("#classifieds_list").append(classified_new); //append received data into the element

                    //hide loading image
                    $('.spinner').hide(); //hide loading image once data is received
                    
                    track_load++; //loaded group increment
                    loading = false; 


                    .fail(function(error) { 
                        
                        $('.spinner').hide(); //hide loading image
                    loading = false;


                    });

                //load data from the server using a HTTP POST request
            }
                
            }
        }
    });
});