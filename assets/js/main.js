var wrapper = $("#wrapper");
var scrollerOptions = {
	cursorcolor: "#FFF",
	cursorborder: "0",
	cursorwidth: "8px",
	cursoropacitymax: "0.1",
	smoothscroll: "true"
};
$(document).ready(function() {

	if(typeof alertify != 'undefined') {
		alertify.set({ labels: {
			ok     : "نعم",
			cancel : "لا"
		} });
	}

	// Sidebar scoller
	if(jQuery().niceScroll) {
		$(".nice-scroll").niceScroll(scrollerOptions);
	}
	
	// Responsive grid
	if(jQuery().isotope) {
		var $container = $("#ads-row");
		$container.isotope({
			isOriginLeft: false,
			layoutMode: 'fitRows',
			itemSelector: '.item-wrapper'
		});
 	}

	// Activate Tooltips
	if(jQuery().tooltip) {
		$('.has-tooltip').tooltip();
	}

	// Forms Validations
	if(jQuery().bootstrapValidator) {
		$('form').bootstrapValidator({ excluded: ':disabled'});
	}
	
	// Sidebar toggling
	$(".menu-toggle").click(function(e) {
		e.preventDefault();
		toggleSidebar();
	});

	// Sidebar scroll to selected
	var selected_subcategory=$('#sidebar-wrapper li.active');
	if(selected_subcategory.length > 0){
	scrollToDiv($('#sidebar-wrapper'),selected_subcategory );
	}
	
	$("#page-content-wrapper").click(function() {
		closeSidebar();
	});

	$(window).resize(function() {
		if ($(window).width() > 768) closeSidebar();
	});


	// Login Animations
	$(".form-group").focusin(function() {
		$(this).find(".glyphicon").animate({
			"opacity": "0"
		}, 200);
	});

	$(".form-group").focusout(function() {
		$(this).find(".glyphicon").animate({
			"opacity": "1"
		}, 300);
	});

	$(".login-form .tab").click(function() {
		var target = $(this).data("toggle");
		$(".login-form .tab").removeClass("selected");
		$(this).addClass("selected");
		$(".login-form .form").addClass("hidden");
		$(".login-form ." + target).removeClass("hidden");
	});

	// Modals
	$(".toggle-modal").click(function() {
		var target = $(this).data("target");
		var tab = $(this).data("tab");
		$("body").addClass("open");
		$(".backdrop").addClass("open");
		if (tab !== null) {
			$(target).addClass("open-modal");
			$("span[data-toggle=" + tab + "]").click();
		}
	});

	$(".backdrop").click(function() {
		$(".open-modal").removeClass("open-modal");
		$("body").removeClass("open");
		$(this).removeClass("open");
	});

	// Strectch images to fit container
	if(jQuery().aspectFill) {
		$(".aspect-fill").aspectFill();
 	}

	// Images lightbox
 	if(jQuery().aspectFill) {
		$('.carousel-inner').magnificPopup({ 
			delegate: '.aspect-fill',
			type: 'image',
			mainClass: 'mfp-with-zoom',

			gallery:{
				enabled:true
			},

			zoom: {
			enabled: true,
			duration: 300, 
			easing: 'ease-in-out',
			opener: function(openerElement) {
				return openerElement.is('img') ? openerElement : openerElement.find('img');
				}
			}
		});
 	}

});

function scrollToDiv(container, target) {
	if (target != undefined)
		container.animate({scrollTop: target.offset().top - 400}, 0);
}

function closeSidebar() {
	wrapper.removeClass("toggled");
}

function toggleSidebar() {
	wrapper.toggleClass("toggled");
}