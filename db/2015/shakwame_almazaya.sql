-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 02, 2015 at 12:15 PM
-- Server version: 5.5.40-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `shakwame_almazaya`
--

-- --------------------------------------------------------

--
-- Table structure for table `a3m_account_facebook`
--

CREATE TABLE IF NOT EXISTS `a3m_account_facebook` (
  `account_id` bigint(20) NOT NULL,
  `facebook_id` bigint(20) NOT NULL,
  `linkedon` datetime NOT NULL,
  PRIMARY KEY (`account_id`),
  UNIQUE KEY `facebook_id` (`facebook_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `a3m_account_facebook`
--

INSERT INTO `a3m_account_facebook` (`account_id`, `facebook_id`, `linkedon`) VALUES
(15, 10154953138305045, '2014-11-19 13:43:54');

-- --------------------------------------------------------

--
-- Table structure for table `a3m_account_twitter`
--

CREATE TABLE IF NOT EXISTS `a3m_account_twitter` (
  `account_id` bigint(20) NOT NULL,
  `twitter_id` bigint(20) NOT NULL,
  `oauth_token` varchar(80) NOT NULL,
  `oauth_token_secret` varchar(80) NOT NULL,
  `linkedon` datetime NOT NULL,
  PRIMARY KEY (`account_id`),
  KEY `twitter_id` (`twitter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `a3m_account_twitter`
--

INSERT INTO `a3m_account_twitter` (`account_id`, `twitter_id`, `oauth_token`, `oauth_token_secret`, `linkedon`) VALUES
(12, 32683450, '32683450-w0upEMmfU9MrQA7qjCO8b3oTO87fyau8SdpREsYGk', 'lujBebBv5peMN1fStizH16B74hLTxtcP7rcOFf3fpNjHn', '2014-11-14 14:29:52');

-- --------------------------------------------------------

--
-- Table structure for table `advertisements`
--

CREATE TABLE IF NOT EXISTS `advertisements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `link` varchar(512) NOT NULL,
  `image` varchar(512) NOT NULL,
  `thumbnail` varchar(128) NOT NULL,
  `phone` varchar(64) NOT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `likes` int(11) NOT NULL DEFAULT '0',
  `dislikes` int(11) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `advertisements`
--

INSERT INTO `advertisements` (`id`, `user_id`, `title`, `description`, `link`, `image`, `thumbnail`, `phone`, `views`, `likes`, `dislikes`, `start_date`, `end_date`, `created_at`) VALUES
(4, 8, '', '<p>\n حلويات</p>\n', 'http://google.com', '7ff95-manaf.png', '', '', 0, 0, 0, '2014-10-30', '2014-11-30', '2014-10-30 20:38:02'),
(6, 8, '', '<p>\n شركة بريل</p>\n', '', '39539-berel.png', '', '', 0, 0, 0, '2014-11-15', '2014-12-27', '2014-11-15 15:20:29'),
(7, 8, '', '', '', '2e8c0-berel.png', '', '', 0, 0, 0, '2014-11-15', '2014-11-29', '2014-11-15 15:28:03'),
(8, 8, '', '', '', '91c3a-mastora.png', '', '66171622', 0, 0, 0, '2014-11-22', '2014-12-31', '2014-11-22 14:57:41');

-- --------------------------------------------------------

--
-- Table structure for table `advertisement_locations`
--

CREATE TABLE IF NOT EXISTS `advertisement_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `advertisement_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `advertisement_locations`
--

INSERT INTO `advertisement_locations` (`id`, `location_id`, `advertisement_id`, `created_at`) VALUES
(5, 1, 4, '2014-10-30 20:38:02'),
(8, 1, 6, '2014-11-15 15:20:29'),
(9, 2, 7, '2014-11-15 15:28:03'),
(10, 2, 8, '2014-11-22 14:57:41');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `color` varchar(128) NOT NULL,
  `icon` text NOT NULL,
  `url` varchar(64) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `section_id`, `name`, `color`, `icon`, `url`, `enabled`) VALUES
(1, 13, 'اوزان', '', '', 'awzan', 1),
(2, 4, 'الملابس', '', '', 'clothes', 1),
(3, 5, 'الكل', '', '', 'all', 1),
(4, 6, 'الكل', '', '', 'all', 1),
(5, 7, 'الكل', '', '', 'all', 1),
(6, 8, 'الكل', '', '', 'all', 1),
(7, 9, 'الكل', '', '', 'all', 1),
(8, 10, 'الكل', '', '', 'all', 1),
(9, 11, 'الكل', '', '', 'all', 1),
(10, 12, 'الكل', '', '', 'all', 1),
(11, 14, 'الكل', '', '', 'all', 1),
(12, 15, 'الكل', '', '', 'all', 1),
(13, 16, 'الكل', '', '', 'all', 1),
(14, 17, 'الكل', '', '', 'all', 1),
(15, 18, 'الكل', '', '', 'all', 1),
(16, 19, 'الكل', '', '', 'all', 1),
(17, 20, 'الكل', '', '', 'all', 1),
(18, 21, 'الكل', '', '', 'all', 1),
(19, 22, 'الكل', '', '', 'all', 1),
(20, 23, 'الكل', '', '', 'all', 1),
(21, 24, 'الكل', '', '', 'all', 1),
(22, 25, 'الكل', '', '', 'all', 1),
(23, 26, 'الكل', '', '', 'all', 1),
(24, 19, 'سيارات مجانا', '', '', 'sayaratfree', 1);

-- --------------------------------------------------------

--
-- Table structure for table `classified_ads`
--

CREATE TABLE IF NOT EXISTS `classified_ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(512) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `phone` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `available` tinyint(1) NOT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `likes` int(11) NOT NULL,
  `dislikes` int(11) NOT NULL,
  `type` enum('vip','regular') NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `classified_ads`
--

INSERT INTO `classified_ads` (`id`, `user_id`, `category_id`, `title`, `description`, `image`, `phone`, `email`, `available`, `views`, `likes`, `dislikes`, `type`, `enabled`, `created_at`) VALUES
(13, 3, 16, '\n ارض زراعية 2فدان و10 قراريط ببهوت المنصورة\n', 'ارض زراعية 2فدان و10 قراريط \nببهوت المنصورة تنتج اربع زراعات\nفي السنة الفدان 450 الف للاستفسار \nت/ 01009558468 - 01019457805', '', '009900', 'issa@gmail.com', 0, 2, 0, 0, 'regular', 1, '2014-10-17 22:23:26'),
(14, 3, 16, ' شركة جرين واى للسياحة', 'ارض زراعية 2فدان و10 قراريط \nببهوت المنصورة تنتج اربع زراعات\nفي السنة الفدان 450 الف للاستفسار \nت/ 01009558468 - 01019457805', '', '009900', 'issa@gmail.com', 0, 8, 0, 0, 'regular', 1, '2014-10-17 22:23:40'),
(16, 3, 16, 'للبيع صيدلية بموقع متميز بمنطقة النعام', 'ارض زراعية 2فدان و10 قراريط \nببهوت المنصورة تنتج اربع زراعات\nفي السنة الفدان 450 الف للاستفسار \nت/ 01009558468 - 01019457805', '', '009900', 'issa@gmail.com', 0, 4, 0, 0, 'regular', 1, '2014-10-17 22:24:01'),
(17, 12, 24, 'اول اعلان باستخدام تويتر', 'اي كلام', '', '12131', 'mirooit@yahoo.com', 0, 0, 0, 0, 'regular', 0, '2014-11-14 19:43:11'),
(24, 8, 2, 'مطعم الدانه المضيئة', 'مطعم الدانه المضيئة', '', '50400334', 'abuzaid123alla@yahoo.com', 0, 16, 1, 0, 'vip', 1, '2014-11-22 15:32:40'),
(25, 8, 2, 'مطعم حدوته مصرية', 'مطعم حدوته مصرية', '', '24747203', 'abuzaid123alla@yahoo.com', 0, 24, 0, 0, 'vip', 1, '2014-11-22 15:40:33'),
(26, 8, 3, 'المصوره ام نادر', 'المصوره ام نادر للافراح', '', '99967199', 'abuzaid123alla@yahoo.com', 0, 5, 0, 0, 'vip', 1, '2014-11-22 17:26:45'),
(27, 8, 3, 'بانوراما للافراح', 'بانوراما للافراح', '', '55951316', 'abuzaid123alla@yahoo.com', 0, 3, 0, 0, 'vip', 1, '2014-11-22 17:28:47'),
(28, 8, 3, 'بانوراما للافراح', 'بانوراما للافراح', '', '55951316', 'abuzaid123alla@yahoo.com', 0, 5, 0, 0, 'vip', 1, '2014-11-22 17:29:25'),
(29, 8, 4, 'دايموند للتجهيزات الغذائية', 'دايموند للتجهيزات الغذائية', '', '24347069', 'abuzaid123alla@yahoo.com', 0, 1, 0, 0, 'vip', 1, '2014-11-22 17:31:24'),
(30, 8, 4, 'مطعم استاذ نقرور', 'مطعم استاذ نقرور للمأكولات البحرية', '', '66989980', 'abuzaid123alla@yahoo.com', 0, 1, 0, 0, 'vip', 1, '2014-11-22 17:33:36'),
(31, 8, 4, 'مطعم باستامنيا', 'مطعم باستامنيا', '', '1828585', 'abuzaid123alla@yahoo.com', 0, 3, 0, 0, 'vip', 1, '2014-11-22 17:35:01'),
(32, 8, 5, 'حلويات دانه شوكليت', 'حلويات دانه شوكليت', '', '24886867', 'abuzaid123alla@yahoo.com', 0, 2, 0, 0, 'vip', 1, '2014-11-22 17:37:04'),
(33, 8, 6, 'عصير خوخ ومشمش', 'عصير خوخ ومشمش', '', '66966627', 'abuzaid123alla@yahoo.com', 0, 3, 0, 0, 'vip', 1, '2014-11-26 06:51:27'),
(34, 8, 6, 'عصائر واحة القرين', 'عصائر واحة القرين', '', '65143319', 'abuzaid123alla@yahoo.com', 0, 2, 0, 0, 'vip', 1, '2014-11-26 07:28:20'),
(35, 8, 7, 'مركز اجيال التعليمي', 'مركز اجيال التعليمي', '', '65747572', 'abuzaid123alla@yahoo.com', 0, 1, 0, 0, 'vip', 1, '2014-11-26 07:30:39'),
(36, 8, 7, 'مركز الاتقان التعليمي', 'مركز الاتقان التعليمي', '', '22460753', 'abuzaid123alla@yahoo.com', 0, 1, 0, 0, 'vip', 1, '2014-11-26 07:53:46'),
(37, 8, 8, 'مركز الطفل', 'مركز الطفل', '', '66424196', 'abuzaid123alla@yahoo.com', 0, 1, 0, 0, 'vip', 1, '2014-11-26 08:00:16'),
(38, 8, 8, 'مركز USA', 'مركز USA', '', '66433173', 'abuzaid123alla@yahoo.com', 0, 5, 0, 0, 'vip', 1, '2014-11-26 08:07:12'),
(39, 8, 9, 'صالون صبايا وبس', 'صالون صبايا وبس', '', '65506388', 'abuzaid123alla@yahoo.com', 0, 2, 0, 0, 'vip', 1, '2014-11-26 08:09:45'),
(40, 8, 9, 'مساج اريام ماجيك', 'مساج اريام ماجيك', '', '66602107', 'abuzaid123alla@yahoo.com', 0, 1, 0, 0, 'vip', 1, '2014-11-26 08:12:23'),
(41, 8, 10, 'عطور ابوللو', 'عطور ابوللو', '', '66046091', 'abuzaid123alla@yahoo.com', 0, 2, 0, 0, 'vip', 1, '2014-11-26 08:13:28'),
(42, 8, 1, 'المركز الدولي', 'المركز الدولي', '', '66672742', 'abuzaid123alla@yahoo.com', 0, 1, 0, 0, 'vip', 1, '2014-11-26 08:16:20'),
(43, 8, 1, 'المركز الدولي للاجهزة ', 'المركز الدولي', '', '66672742', 'abuzaid123alla@yahoo.com', 0, 1, 0, 0, 'vip', 1, '2014-11-26 08:18:25'),
(44, 8, 1, 'المركز العربي للتخسيس', 'المركز العربي للتخسيس', '', '50642704', 'abuzaid123alla@yahoo.com', 0, 1, 0, 0, 'vip', 1, '2014-11-26 08:20:15'),
(45, 8, 11, 'مستوصف الصباح', 'مستوصف الصباح', '', '23920151', 'abuzaid123alla@yahoo.com', 0, 0, 0, 0, 'vip', 1, '2014-11-26 08:21:20'),
(46, 8, 11, 'مستوصف الصباح', 'مستوصف الصباح', '', '23920151', 'abuzaid123alla@yahoo.com', 0, 2, 0, 0, 'vip', 1, '2014-11-26 08:24:12'),
(47, 8, 11, 'صيدلية جمعية مبارك الكبير', 'صيدلية جمعية مبارك الكبير', '', '25434012', 'abuzaid123alla@yahoo.com', 0, 1, 0, 0, 'vip', 1, '2014-11-26 08:25:05'),
(48, 8, 16, 'مبوب سيارات', 'للبيع سيارة موديل 2011 ماشى 11 الف صبغ الوكالة', '', '50400334', 'abuzaid123alla@yahoo.com', 0, 0, 0, 0, 'regular', 0, '2014-12-02 15:55:23');

-- --------------------------------------------------------

--
-- Table structure for table `classified_ad_images`
--

CREATE TABLE IF NOT EXISTS `classified_ad_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classified_ad_id` int(11) NOT NULL,
  `image` varchar(512) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `classified_ad_id` (`classified_ad_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `classified_ad_images`
--

INSERT INTO `classified_ad_images` (`id`, `classified_ad_id`, `image`, `created_at`) VALUES
(29, 24, '8db4c15b57009780c3259ab924590c0e.png', '2014-11-22 15:34:49'),
(30, 25, '756a5aad8faacff3dd9974767619d97b.png', '2014-11-22 15:40:52'),
(31, 26, '213c7993708ba769bfb19060156eab71.png', '2014-11-22 17:26:58'),
(32, 28, 'd3b7c6760e20bff8dfbc5230bb1c647a.png', '2014-11-22 17:29:39'),
(33, 29, '24caff0d54fd4d5200a777c13e5915d9.png', '2014-11-22 17:32:03'),
(34, 30, 'b8d6b83f39752832f00315d9862dafcb.png', '2014-11-22 17:33:51'),
(35, 31, 'e19e12c7cbc1c7eaeb4150ed2be57dc2.png', '2014-11-22 17:35:41'),
(36, 32, 'd0810bdd33e57c30e9f024d37ce0570b.png', '2014-11-22 17:38:21'),
(37, 33, '9bd9c8bb8516066f0023d8e2fe0d9a2f.png', '2014-11-26 06:51:47'),
(38, 34, '47f4ba59f6f7788517af85a105d76b2c.png', '2014-11-26 07:28:49'),
(39, 35, '44563a2d959d09db7902aa5580488877.png', '2014-11-26 07:30:51'),
(40, 36, '0d589b9356dd76f02aa17ca9db460f5d.png', '2014-11-26 07:54:07'),
(41, 37, '610cf4373523a703b60925933cb7280b.png', '2014-11-26 08:00:48'),
(42, 38, 'dbaf6bd28026d78a5dad933be2f26171.png', '2014-11-26 08:07:25'),
(43, 39, 'd833dd6856481fffc8ca326c2214ad29.png', '2014-11-26 08:10:01'),
(44, 40, '718b6a819e7b69cc68c5c28da98545db.png', '2014-11-26 08:12:39'),
(45, 41, 'a3c1f6c846d116b59f6d42e9a5c8c23b.png', '2014-11-26 08:14:23'),
(46, 43, '3d624fa20b365fe3d30e2390ad17c6cf.png', '2014-11-26 08:18:36'),
(47, 44, '3e9894238413caaaa856168d1488db8a.png', '2014-11-26 08:20:29'),
(48, 46, 'dc2e3892d251cdc97a6b859a70a4bf8a.png', '2014-11-26 08:24:22'),
(49, 47, '46ac3c12b38745da02547f319c903227.png', '2014-11-26 08:26:18');

-- --------------------------------------------------------

--
-- Table structure for table `classified_ad_issues`
--

CREATE TABLE IF NOT EXISTS `classified_ad_issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classified_ad_id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `classified_ad_issues`
--

INSERT INTO `classified_ad_issues` (`id`, `classified_ad_id`, `issue_id`, `created_at`) VALUES
(5, 3, 3, '2014-09-08 13:20:55'),
(9, 7, 3, '2014-09-17 19:21:39'),
(15, 13, 3, '2014-10-17 22:23:26'),
(16, 14, 3, '2014-10-17 22:23:40'),
(17, 15, 3, '2014-10-17 22:23:52'),
(18, 16, 3, '2014-10-17 22:24:01'),
(19, 17, 3, '2014-11-14 19:43:11'),
(26, 24, 3, '2014-11-22 15:32:40'),
(27, 25, 3, '2014-11-22 15:40:33'),
(28, 26, 3, '2014-11-22 17:26:45'),
(29, 27, 3, '2014-11-22 17:28:47'),
(30, 28, 3, '2014-11-22 17:29:25'),
(31, 29, 3, '2014-11-22 17:31:24'),
(32, 30, 3, '2014-11-22 17:33:36'),
(33, 31, 3, '2014-11-22 17:35:01'),
(34, 32, 3, '2014-11-22 17:37:04'),
(35, 33, 3, '2014-11-26 06:51:27'),
(36, 34, 3, '2014-11-26 07:28:20'),
(37, 35, 3, '2014-11-26 07:30:39'),
(38, 36, 3, '2014-11-26 07:53:46'),
(39, 37, 3, '2014-11-26 08:00:16'),
(40, 38, 3, '2014-11-26 08:07:12'),
(41, 39, 3, '2014-11-26 08:09:45'),
(42, 40, 3, '2014-11-26 08:12:23'),
(43, 41, 3, '2014-11-26 08:13:28'),
(44, 42, 3, '2014-11-26 08:16:20'),
(45, 43, 3, '2014-11-26 08:18:25'),
(46, 44, 3, '2014-11-26 08:20:15'),
(47, 45, 3, '2014-11-26 08:21:20'),
(48, 46, 3, '2014-11-26 08:24:12'),
(49, 47, 3, '2014-11-26 08:25:05'),
(50, 48, 3, '2014-12-02 15:55:25');

-- --------------------------------------------------------

--
-- Table structure for table `facebook_session_bugs`
--

CREATE TABLE IF NOT EXISTS `facebook_session_bugs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `provider_id` bigint(20) NOT NULL,
  `provider` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `fullname` varchar(256) NOT NULL,
  `firstname` varchar(256) NOT NULL,
  `lastname` varchar(256) NOT NULL,
  `gender` varchar(32) NOT NULL,
  `picture` varchar(256) NOT NULL,
  `dateofbirth` varchar(32) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `facebook_session_bugs`
--

INSERT INTO `facebook_session_bugs` (`id`, `provider_id`, `provider`, `email`, `fullname`, `firstname`, `lastname`, `gender`, `picture`, `dateofbirth`, `created_at`) VALUES
(1, 10153315333658102, 'facebook', 'mirooit@yahoo.com', 'Sara Ramzy', 'Sara', 'Ramzy', 'female', 'http://graph.facebook.com/10153315333658102/picture/?type=large', '', '2014-11-14 19:43:38'),
(2, 10153315333658102, 'facebook', 'mirooit@yahoo.com', 'Sara Ramzy', 'Sara', 'Ramzy', 'female', 'http://graph.facebook.com/10153315333658102/picture/?type=large', '', '2014-11-16 10:49:21'),
(3, 10154953138305045, 'facebook', 'cg.karimtarek@gmail.com', 'Karim Tarek', 'Karim', 'Tarek', 'male', 'http://graph.facebook.com/10154953138305045/picture/?type=large', '', '2014-11-19 18:43:43'),
(4, 746355672105894, 'facebook', 'issa.haddad.fb@gmail.com', 'Haddad Issa', 'Haddad', 'Issa', 'male', 'http://graph.facebook.com/746355672105894/picture/?type=large', '', '2014-12-02 06:15:23');

-- --------------------------------------------------------

--
-- Table structure for table `issues`
--

CREATE TABLE IF NOT EXISTS `issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `number` varchar(64) NOT NULL,
  `pro_date` date NOT NULL,
  `pub_date` date NOT NULL,
  `pdf` varchar(256) NOT NULL,
  `invoice` varchar(256) NOT NULL COMMENT 'print invoice',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `issues`
--

INSERT INTO `issues` (`id`, `name`, `number`, `pro_date`, `pub_date`, `pdf`, `invoice`, `created_at`) VALUES
(3, 'العدد ٢٢٠', '220', '2014-09-02', '2014-09-07', '2913d-pdf-sample.pdf', '6f58e-pdf-sample.pdf', '2014-09-02 10:17:53');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `created_at`) VALUES
(1, 'الصفحة الرئيسية 1', '2014-09-27 10:39:25'),
(2, 'الصفحة الرئيسية 2', '2014-09-27 10:39:43'),
(3, 'داخل الإعلان', '2014-10-17 22:04:35'),
(4, 'الاعلانات المميزة', '2014-11-22 15:00:03'),
(5, 'آيفون و أندرويد', '2014-12-16 19:13:12');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `icon` text NOT NULL,
  `url` varchar(64) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `priority` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `name`, `icon`, `url`, `enabled`, `priority`) VALUES
(4, 'اعلانات مميزة', '180c7-vip.png', 'vip', 1, 2),
(5, 'افراح', '3074e-afraah.png', 'afrah', 1, 4),
(6, 'مطاعم', '65c64-rest.png', 'Restaurants', 1, 5),
(7, 'حلويات', 'e5a80-sweet.png', 'Sweets', 1, 6),
(8, 'عصائر', 'a1eef-aseer.png', 'Juices', 1, 7),
(9, 'معاهد تعليمية', '0928f-maad.png', 'Educational', 1, 8),
(10, 'العاب', '5f9fd-games.png', 'Games', 1, 9),
(11, 'صالونات نسائي', '44cd8-saloon.png', 'Salon', 1, 10),
(12, 'عطور واكسسوارات', '0f752-burfane.png', 'Perfumes', 1, 11),
(13, 'اجهزة رياضية وتخسيس', '54975-agheza.png', 'Sports', 1, 12),
(14, 'عيادات طبية وتجميل', 'ae871-clinic.png', 'clinics', 1, 13),
(15, 'اسواق شعبية', 'cae0e-aswaaq.png', 'Markets', 1, 14),
(16, 'ديباجات ومفروشات', 'bbf94-mafroshat.png', 'Furniture', 1, 15),
(17, 'سيارات', '24588-cars.png', 'cars', 1, 16),
(18, 'حملات الحج والعمره', '0e0d7-hag.png', 'omra', 1, 17),
(19, 'اعلانات مبوبة', 'df931-mopawap.png', 'mobawab', 1, 3),
(20, 'تليفونات', '0414d-manzel.png', 'telephone', 1, 18),
(21, 'تنجيد وستائر', 'e49f4-tangeed.png', 'tangeed', 1, 23),
(22, 'ستلايت', 'b8225-satalit.png', 'Satellite', 1, 22),
(23, 'نقل عفش ونشتري', 'c0927-naql.png', 'naql', 1, 21),
(24, 'مقاولات عامة', 'c7c45-moqawlat.png', 'moqawel', 1, 20),
(25, 'تنظيف ومكافحة حشرات', '35cc4-tanzeef.png', 'Clean', 1, 19),
(26, 'صيانة مكيفات', '95813-aircondition.png', 'conditioning', 1, 24);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `password` varchar(64) NOT NULL,
  `name` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL,
  `phone` varchar(128) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:disabled , 1:enabled',
  `role` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:normal,1:admin',
  `provider` enum('default','twitter','facebook') NOT NULL DEFAULT 'default' COMMENT 'registeration meduim',
  `lastsignedinon` datetime NOT NULL,
  `resetsenton` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `image`, `phone`, `enabled`, `role`, `provider`, `lastsignedinon`, `resetsenton`, `created_at`) VALUES
(1, 'a@gmail.com', '202cb962ac59075b964b07152d234b70', 'ahmed سعيد', '', '123456', 1, 0, 'default', '0000-00-00 00:00:00', NULL, '2014-07-12 04:54:41'),
(2, '7amo@gmail.com', '202cb962ac59075b964b07152d234b70', '7amo', '', '1828', 1, 0, 'default', '0000-00-00 00:00:00', NULL, '2014-07-12 05:21:14'),
(3, 'issa@gmail.com', '920690f41b732226ca4e7fde568da9ee', 'issahaddad', '66a620541898712747bfc8316e23344f.png', '009900', 1, 1, 'default', '2015-01-02 04:53:33', NULL, '2014-07-12 08:51:30'),
(4, 'test@gmail.com', '202cb962ac59075b964b07152d234b70', 'moh', '', '123', 1, 0, 'default', '0000-00-00 00:00:00', NULL, '2014-07-31 18:24:25'),
(5, 'johny.ovik@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'karim', '', '123456', 1, 0, 'default', '0000-00-00 00:00:00', NULL, '2014-08-02 23:02:07'),
(6, 'tormentor308@gmail.com', 'b78fb5a9cf2e185f37eb6f545cdbfc92', 'Shady Emil', '', '111793177', 1, 1, 'default', '0000-00-00 00:00:00', NULL, '2014-08-22 21:38:45'),
(7, 'tormentor309@gmail.com', 'b78fb5a9cf2e185f37eb6f545cdbfc92', '308tormentor308', '', '456874', 1, 0, 'default', '0000-00-00 00:00:00', NULL, '2014-08-23 13:51:49'),
(8, 'abuzaid123alla@yahoo.com', 'b365f230f5e64205c43a868135345ebd', 'abuzaid', '', '50400334', 1, 1, 'default', '2014-12-02 10:54:40', NULL, '2014-08-31 15:31:43'),
(9, 'ali.ahmed@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'أحمد علي', '7b0b5792e34f6848630547b0c21e9966.jpg', '01111532514', 1, 1, 'default', '0000-00-00 00:00:00', NULL, '2014-09-05 14:27:15'),
(10, 'isso@gmail.com', 'fcbf3b5dd5bc6ed65ef1ecad830e1825', 'issooo', 'ffcf8a09022e557950c96c7a382ce1f9.jpg', '323232', 1, 0, 'default', '0000-00-00 00:00:00', NULL, '2014-09-17 06:52:16'),
(11, 'io@yahoo.com', '202cb962ac59075b964b07152d234b70', 'ioio', '65823007b837406a50ba4156dc774a93.jpg', '123', 1, 0, 'default', '0000-00-00 00:00:00', NULL, '2014-09-18 06:24:01'),
(12, 'mirooit@yahoo.com', '', 'xtarx', '', '12131', 1, 0, 'twitter', '2014-11-16 05:50:46', NULL, '2014-11-14 19:29:52'),
(14, 'mirooitfb@yahoo.com', '', 'Sara Ramzy', '', '1211', 1, 0, 'facebook', '2014-11-14 14:49:27', NULL, '2014-11-14 19:47:36'),
(15, 'cg.karimtarek@gmail.com', '', 'Karim Tarek', '', '01111231322', 1, 0, 'facebook', '2014-12-24 12:39:58', NULL, '2014-11-19 18:43:54'),
(16, 'issa.haddad.fb@gmail.com', '', 'Haddad Issa', '', '50485225', 1, 0, 'facebook', '0000-00-00 00:00:00', NULL, '2014-12-02 06:15:43');

-- --------------------------------------------------------

--
-- Table structure for table `user_actions`
--

CREATE TABLE IF NOT EXISTS `user_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `classified_ad_id` int(11) NOT NULL,
  `action` int(11) NOT NULL DEFAULT '0' COMMENT '0:like',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `user_actions`
--

INSERT INTO `user_actions` (`id`, `user_id`, `classified_ad_id`, `action`, `created_at`) VALUES
(9, 6, 5, 0, '2014-08-22 22:04:10'),
(10, 6, 4, 0, '2014-08-22 23:15:54'),
(17, 8, 2, 0, '2014-08-31 15:42:40'),
(19, 3, 2, 0, '2014-08-31 15:50:27'),
(20, 6, 3, 0, '2014-09-17 13:27:58'),
(21, 9, 12, 0, '2014-09-17 19:27:59'),
(22, 9, 9, 0, '2014-09-17 19:28:07'),
(23, 12, 11, 0, '2014-11-14 19:43:24'),
(25, 15, 12, 0, '2014-11-19 18:44:42'),
(27, 3, 24, 0, '2014-12-13 06:28:03');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `classified_ad_images`
--
ALTER TABLE `classified_ad_images`
  ADD CONSTRAINT `classified_ad_images_ibfk_1` FOREIGN KEY (`classified_ad_id`) REFERENCES `classified_ads` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
