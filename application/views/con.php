
<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">

<style type="text/css">
body {width: 1000px; background: #fff;}

#myCarousel {margin-left: 30px; margin-right: 30px;}

.carousel-control {
  top: 50%;
}
.carousel-inner {
  width: 940px;
  height: 120px;
}
.carousel-control.left, .carousel-control.right {
  background: none;
  color: @red;
  border: none;
}
.carousel-control.left {margin-left: -45px; color: black;}
.carousel-control.right {margin-right: -45px; color: black;}

</style>


<body>

<div class="item">
  <img src="..." alt="...">
  <div class="carousel-caption">
    <h3>...</h3>
    <p>...</p>
  </div>
</div>

  <div id="myCarousel" class="carousel slide"> <!-- slider -->
    <div class="carousel-inner">
      <div class="active item"> <!-- item 1 -->
        <img src="http://placehold.it/940x120" alt="">
      </div> <!-- end item -->
      <div class="item"> <!-- item 2 -->
        <img src="http://placehold.it/940x120" alt="">
      </div> <!-- end item -->
      <div class="item"> <!-- item 3 -->
        <img src="http://placehold.it/940x120" alt="">
      </div> <!-- end item -->
    </div> <!-- end carousel inner -->
    <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
    <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
  </div> <!-- end slider -->
</body>               
<script src="<?php echo base_url('assets') ?>/js/vendor/jquery-1.11.0.min.js"></script>

<?php   $this->load->view("footer"); ?>


<script type="text/javascript">
$(window).load(function() {
  $('.carousel').carousel()
  $('#myCarousel').carousel({
    interval: 3000
  })
});

</script>