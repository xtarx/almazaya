<?php   $this->load->view("user/header"); ?>
<link rel="stylesheet" href="<?php echo base_url('assets') ?>/css/registration.css">

<div id="wrapper">

  <!-- Login & Register Forms -->
  <div id="login-form" class="col-centered col-sm-4 login-form">
    <!-- Login & Register Tabs -->
    <div class="login-header row">
     <span class="tab col-xs-6 selected" data-toggle="signIn-form">استكمال التسجيل</span>
   </div>


   <!-- Register Form -->
   <form class="form signUp-form row " role="form" method="post"  action="<?= base_url('account/connect_create/do_facebook')?>" >

    <!-- erros div   -->
    <?php if ((validation_errors())): ?>
    <div class="red"  >
     <?php echo validation_errors(); ?>
   </div>
 <?php endif; ?>

 <!-- erros div   -->



<div class="col-lg-12">

  <div class="form-group inner-addon right-addon">
    <label class="sr-only" for="exampleInputPass">رقم الهاتف</label>
    <i class="glyphicon glyphicon-earphone"></i>
    <input type="text" class="form-control" id="exampleInputPhone" placeholder="رقم الهاتف" name="phone"
    data-bv-notempty="true" 
    data-bv-notempty-message="رقم الهاتف لا يمكن أن يكون فارغ"

    data-bv-numeric="true" 
    data-bv-numeric-message="رقم الهاتف يحتوي علي ارقام فقط"

    >
  </div>
  <button type="submit" class="btn wide-btn btn-primary btn-lrg">استكمال</button>
</div>

</form>
<!-- Register Form -->



</div>

</div><!-- /#wrapper -->

<?php   $this->load->view("user/footer"); ?>



<script src="<?php echo base_url('assets') ?>/js/registration.js"></script>
