<link rel="stylesheet" href="<?php echo base_url('assets') ?>/css/registration.css">

<!-- Login & Register Forms -->
<div id="login-form" class="col-centered col-sm-4 login-form">
  <!-- Login & Register Tabs -->
  <div class="login-header row">
   <span class="tab col-xs-6 selected" data-toggle="signIn-form">الدخول</span>
   <span class="tab col-xs-6" data-toggle="signUp-form">حساب جديد</span>
 </div>


 <!-- Login Form -->
 <form class="form signIn-form" action="<?= base_url("user/do_login")?>" method="post" role="form">
  <!-- social buttons -->
  <?php   $this->load->view("account/social_buttons"); ?>
  <!-- social buttons -->

  <div class="form-group inner-addon right-addon">
    <label class="sr-only" for="exampleInputEmail">البريد الإلكتروني</label>
    <i class="glyphicon glyphicon-envelope"></i>
    <input type="email" class="form-control" id="exampleInputEmail" placeholder="البريد الإلكتروني" name="email"

    data-bv-notempty="true" 
    data-bv-notempty-message="البريد الإلكتروني لا يمكن أن يكون فارغ"
    data-bv-emailaddress="true"
    data-bv-emailaddress-message="البريد الإلكتروني غير صالح"
    >
  </div>

  <div class="form-group inner-addon right-addon">
    <label class="sr-only" for="exampleInputPass">كلمة المرور</label>
    <i class="glyphicon glyphicon-lock"></i>
    <input type="password" class="form-control" id="exampleInputPass" placeholder="كلمة المرور" name="password">
  </div>
<a href="<?php echo base_url('user/forgot_password')?>">نسيت كلمة المرور؟</a>
  <button type="submit" class="btn wide-btn btn-primary btn-lrg">تسجيل الدخول</button>
</form>
<!-- Login Form -->


<!-- Register Form -->
<form class="form signUp-form row hidden" role="form" method="post"   action="<?= base_url("user/do_register")?>"  enctype="multipart/form-data">

  <!-- social buttons -->
  <?php   $this->load->view("account/social_buttons"); ?>
  <!-- social buttons -->


  <?php if ((validation_errors())): ?>
  <div class="red"  >
   <?php echo validation_errors(); ?>
 </div>
<?php endif; ?>


<div class="col-lg-7">
  <div class="form-group inner-addon right-addon">
    <label class="sr-only" for="exampleInputUser">اسم المستخدم</label>
    <i class="glyphicon glyphicon-user"></i>
    <input type="text" class="form-control" id="exampleInputUser" placeholder="اسم المستخدم" name="name"

    data-bv-notempty="true" 
    data-bv-notempty-message="اسم المستخدم لا يمكن أن يكون فارغ"
    data-bv-stringlength="true"
    data-bv-stringlength-max="30"
    data-bv-stringlength-message="يجب أن يكون اسم المستخدم أقل من 30 حرف"

    >
  </div>
  <div class="form-group inner-addon right-addon">
    <label class="sr-only" for="exampleInputEmail">البريد الإلكتروني</label>
    <i class="glyphicon glyphicon-envelope"></i>
    <input type="email" class="form-control" id="exampleInputEmail" placeholder="البريد الإلكتروني" name="email"
    data-bv-notempty="true" 
    data-bv-notempty-message="البريد الإلكتروني لا يمكن أن يكون فارغ"
    data-bv-emailaddress="true"
    data-bv-emailaddress-message="البريد الإلكتروني غير صالح"

    >
  </div>
</div>

<div class="col-lg-5" >
  <div id="dropzone">
    <div class="dropzone_NOT">ضع صورتك هنا</div>
    <input type="file"  name="userfile" accept="image/png, image/jpeg, image/jpg" />            

  </div>
</div>

<div class="col-lg-12">
  <div class="form-group inner-addon right-addon">
    <label class="sr-only" for="exampleInputPass">كلمة المرور</label>
    <i class="glyphicon glyphicon-lock"></i>
    <input type="password" class="form-control" id="exampleInputPass" placeholder="كلمة المرور" name="password"

    data-bv-notempty="true" 
    data-bv-notempty-message="كلمة المرور لا يمكن أن يكون فارغ"
    data-bv-stringlength="true"
    data-bv-stringlength-max="30"
    data-bv-stringlength-message="يجب أن يكون كلمة المرور أقل من 30 حرف"

    >
  </div>
  <div class="form-group inner-addon right-addon">
    <label class="sr-only" for="exampleInputPass">رقم الهاتف</label>
    <i class="glyphicon glyphicon-earphone"></i>
    <input type="text" class="form-control" id="exampleInputPhone" placeholder="رقم الهاتف" name="phone"
    data-bv-notempty="true" 
    data-bv-notempty-message="رقم الهاتف لا يمكن أن يكون فارغ"
    data-bv-numeric="true" 
    data-bv-numeric-message="رقم الهاتف يحتوي علي ارقام فقط"


    >
  </div>
  <button type="submit" class="btn wide-btn btn-primary btn-lrg">انشاء حساب</button>
</div>

</form>
<!-- Register Form -->

</div>
<div class="backdrop"></div>
