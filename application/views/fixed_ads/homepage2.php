<?php $size=count($fixed_ads_homepage_2);?>

<div class="col-md-12 col-sm-6">
	<div class="adplaceholder">
		<div class="ad-carousel ad-2 carousel slide" data-ride="carousel">
			

	<!-- Wrapper for slides -->
	<div class="carousel-inner">
		<div class="item active">
			<a href="<?php echo $fixed_ads_homepage_2[0]['link']; ?>" >   <img src="<?php echo base_url('uploads/static_ads/'.$fixed_ads_homepage_2[0]['image']) ?>" ></a>
		</div>
		<?php for ($i=1; $i <$size ; $i++): ?>

		<div class="item">
			<a href="<?php echo $fixed_ads_homepage_2[$i]['link']; ?>" >   <img src="<?php echo base_url('uploads/static_ads/'.$fixed_ads_homepage_2[$i]['image']) ?>" ></a>
		</div>

	<?php  endfor; ?>
</div>
<?php if($size>1): ?>

	<!-- Controls -->
	<a class="left carousel-control" href=".ad-carousel.ad-2" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
	</a>
	<a class="right carousel-control" href=".ad-carousel.ad-2" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
	</a>
	<!-- Controls -->
<?php  endif; ?>

</div>
</div>
</div>