<?php   $this->load->view("header"); ?>

<!-- dropzone css -->
<link href="<?php echo base_url(); ?>resouces/css/dropzone.css" type="text/css" rel="stylesheet" />
<!-- dropzone css -->
<div id="page-content-wrapper">
  <div class="container-fluid">
    <div class="row">

      <!-- Filtering & Search -->
      <div class="filter-header col-lg-12">
        <h2 class="category-title">إضافة الصور</h2>
        <div class="separator"></div>
      </div>

      <!-- Images List -->
      <div class="col-lg-12 margin-top">

        <!-- image upload -->
        <div  id="uploadfiles" action="<?php echo base_url('classified_ads/do_upload/'.$this->uri->segment(3)); ?>" class="dropzone"  >
        </div>
        </div> <!-- /Ads List -->
        
        <div class="col-lg-6 pull-left submit">
          <input id="submit_images" type="submit" name=""  value="حفظ" class="btn btn-danger btn-lg pull-left col-sm-4" data-loading-text="الرجاء الإنتظار">
          
        </div>
        <!-- image upload -->
  </div>
</div>

</div><!-- /#page-content-wrapper -->
<?php   $this->load->view("footer"); ?>

<!-- dropzonejs -->
<script src="<?php echo base_url(); ?>resouces/dropzone.min.js"></script>

<script>
Dropzone.options.uploadfiles = {
  init: function () {
        // alert("start")

       // $("#submit_images").prop('disabled', true);

       this.on("complete", function (file) {
        if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {

          $("#submit_images").prop('disabled', false);


        } 
      });
     }
   };


   $(function() {
    $("#submit_images").click(function(e) {

      window.location ="<?php echo base_url('classified_ads/view/'.$this->uri->segment(3)) ?>"
    });
  });

   </script>
   <!-- dropzonejs -->
