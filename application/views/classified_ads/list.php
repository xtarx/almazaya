<?php   $this->load->view("header"); ?>
<!-- horizontal ticker -->
<link rel="stylesheet" href="<?php echo base_url('assets') ?>/css/li-scroller.css">
<link rel="stylesheet" href="<?php echo base_url('assets') ?>/css/carousel.css">

<!-- horizontal ticker -->    


<!-- Page Content -->
<div id="page-content-wrapper">
  <div class="container-fluid">
    <div class="row">

      <!-- Filtering & Search -->
      <div class="filter-header col-lg-12">
        <h2 class="category-title"><?php echo $selected_section_name?> 
          <?php if(isset($selected_category_name) ):?>
          - <?php echo $selected_category_name ?>
        <?php endif; ?>
      </h2>
      <div class="separator"></div>
      <div class="row">
        <div class="tags col-lg-8">
          <a href="<?php echo current_url() ?>?sort=recent"><span class="tag <?php if(!$this->input->get('sort')||$this->input->get('sort')=="recent"):?> selected <?php endif; ?>">الأحدث</span></a>
          <a href="<?php echo current_url() ?>?sort=views"><span class="tag <?php if($this->input->get('sort')=="views"):?> selected <?php endif; ?>">الاكثر مشاهدة</span></a>
        </div>
        <div class="searchbar col-lg-4">
          <form class="navbar-form pull-left" role="search">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="كلمات البحث" value="<?php echo $this->input->get('q'); ?>" name="q" id="srch-term">
              <div class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="separator"></div>
      <!-- horizontal ticker -->    
</br>

      <ul id="ticker01">
        <?php        
        foreach ($mobawabas as $key => $value):?>
        <li><a href="<?php echo base_url('classified_ads/view/'.$value['id']) ?>"><?php echo  justify_string($value['title'],140) ?></a></li>
      <?php endforeach; ?>
      <!-- eccetera -->
    </ul>
    <!-- horizontal ticker -->    


    <div class="separator"></div>
  </div>

  <!-- Payed Ads -->
  <div class="margin-top col-md-3 visible-sm-block visible-xs-block">
    <div class="row">
      <!-- One Payed Ad -->
      <!-- homepage1 -->
      <?php if(isset($fixed_ads_homepage_1)&&!empty($fixed_ads_homepage_1)): ?>
      <?php   $this->load->view("fixed_ads/homepage1",$fixed_ads_homepage_1); ?>
    <?php endif; ?>
    <!-- /homepage1 -->

    <?php if(isset($fixed_ads_homepage_2) &&!empty($fixed_ads_homepage_2)): ?>

    <!-- homepage2 -->
    <?php   $this->load->view("fixed_ads/homepage2",$fixed_ads_homepage_2); ?>

    <!-- /homepage2 -->
  <?php endif; ?>

</div>
</div>
<!-- /Payed Ads -->

<!-- Ads List -->
<div class="AdsList margin-top col-md-9" id="classifieds_list">
  <!-- Row -->
  <div class="row" id="ads-row">
    <?php foreach ($ads as $value): ?>

    <!-- Ad Card -->
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 item-wrapper">
      <div class="ad-element">
       <a href="<?php echo base_url('classified_ads/view/'.$value['id']) ?>" >
        <?php


        if(!is_mobawaba()):?>
        <!-- in case mobawab dont show -->
        <div class="ad-photo" style="background-image: url('<? echo base_url('uploads/classified_ads/images/'. $value['image']) ?>');">
          <div class="overlay"><span class="glyphicon glyphicon-eye-open"></span></div>
        </div>
      <?php endif; ?>

      <!-- in case mobawab dont show -->
    </a>
    <div class="ad-details row">
      <div class="col-xs-12">
        <a href="<?php echo base_url('classified_ads/view/'.$value['id']) ?>" class="ad-title"><?php   echo justify_string($value['title'],140); ?></a>
        <div class="ad-owner"><?php echo justify_string($value['description'],48) ?></div>
      </div>
      <!-- <div class="ad-price col-xs-4">7.4 $</div> -->
    </div>
    <div class="separator"></div>
    <div class="ad-details row">
      <div class="col-xs-6"><span class="glyphicon glyphicon-thumbs-up"></span><?php echo $value['likes'] ?></div>
      <div class="col-xs-6"><span class="glyphicon glyphicon-eye-open"></span><?php echo $value['views'] ?></div>
    </div>
  </div>
</div>

<?php endforeach; ?>
</div><!-- /Row-->
</div> <!-- /Ads List -->

<!-- Payed Ads -->
<div class="margin-top col-md-3 hidden-sm hidden-xs">
  <div class="row">
    <!-- One Payed Ad -->
    <!-- homepage1 -->

    <?php if(isset($fixed_ads_homepage_1)&&!empty($fixed_ads_homepage_1)): ?>
    <?php   $this->load->view("fixed_ads/homepage1",$fixed_ads_homepage_1); ?>

  <?php endif; ?>
  <!-- /homepage1 -->
  <!-- homepage2 -->
  <?php if(isset($fixed_ads_homepage_2)&&!empty($fixed_ads_homepage_2)): ?>
  <?php   $this->load->view("fixed_ads/homepage2",$fixed_ads_homepage_2); ?>
  <!-- /homepage2 -->
<?php endif; ?>

</div>
</div>
<!-- /Payed Ads -->


<!-- Spinner Loader -->
<div class="spinner" style=" display: none; ">
  <div class="bounce3"></div>
  <div class="bounce2"></div>
  <div class="bounce1"></div>
</div>

</div>
</div>

</div><!-- /#page-content-wrapper -->


<?php   $this->load->view("footer"); ?>
<script src="<?php echo base_url('assets') ?>/js/vendor/isotope.pkgd.min.js"></script>
<!-- horizontal ticker -->
<script src="<?php echo base_url('assets') ?>/js/jquery.li-scroller.1.0.js"></script>
<script src="<?php echo base_url('assets') ?>/js/vendor/jquery.carousel.min.js"></script>
<script type="text/javascript">
$(function(){
  $("ul#ticker01").liScroll();
});
</script>
<!-- horizontal ticker -->    

<!-- load infite scroll -->    
<!-- a<script src="<?= base_url(); ?>assets/js/scroll.js"></script>  -->
<!-- load infite scroll -->
