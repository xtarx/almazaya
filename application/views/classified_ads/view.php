<?php   $this->load->view("header"); ?>
<link rel="stylesheet" href="<?php echo base_url('assets') ?>/css/carousel.css">
<link rel="stylesheet" href="<?php echo base_url('assets') ?>/css/magnific-popup.css">

<!-- Page Content -->
<div id="page-content-wrapper">
  <div class="container-fluid">
    <div class="row">

      <!-- Filtering & Search -->
      <div class="filter-header col-lg-12">
        <h2 class="category-title"><?php echo $title ?></h2>
        <div class="separator"></div>
        <div class="row">
          <div class="tags col-lg-9">
            <a href="<?php echo base_url("classified_ads/list_ads/".$section_url); ?>"><span class="tag"><?php echo $section_name ?></span></a>
            <i>></i>
            <a href="<?php echo base_url("classified_ads/list_ads/".$section_url.'/'    .$category_url ); ?>"> <span class="tag"><?php echo $category_name ?></span></a>
          </div>

          <div class="col-lg-3 edit-ad">            
            <?php if($is_owner): ?>
            <!-- extra privs -->
            <a id="delete-link" class="circle-icon has-tooltip" href="#" data-toggle="tooltip" data-placement="top" title="حذف"><span class="glyphicon glyphicon-remove"></span></a>
            <a id="edit-link" class="circle-icon has-tooltip" href="<?php echo base_url("classified_ads/edit/".$this->uri->segment(3)) ?>" data-toggle="tooltip" data-placement="top" title="تعديل"><span class="glyphicon glyphicon-edit"></span></a>
            <!-- extra privs -->
          <?php endif; ?>
        </div>
      </div>
      <div class="separator"></div>
    </div>

    <!-- Ad View -->
    <div class="AdView margin-top col-lg-12">
      <!-- Row -->
      <div class="row">

      <?php  if(!empty($classified_ad_images)): ?>
        <!-- Ad Photos -->
        <div class="col-lg-4 ad-photos">

          <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
              <?php for ($i=1; $i <count($classified_ad_images) ; $i++): ?>
              <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i ?>"></li>
              <?php  endfor; ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
              <div class="item active">
                <img class="pull-left aspect-fill" href="<?php echo base_url('uploads/classified_ads/images/'.$classified_ad_images[0]['image']) ?>" src="<?php echo base_url('uploads/classified_ads/images/'.$classified_ad_images[0]['image']) ?>" >
              </div>

              <?php for ($i=1; $i <count($classified_ad_images) ; $i++): ?>

              <div class="item">
                <img class="pull-left aspect-fill" href="<?php echo base_url('uploads/classified_ads/images/'.$classified_ad_images[$i]['image']) ?>" src="<?php echo base_url('uploads/classified_ads/images/'.$classified_ad_images[$i]['image']) ?>" >
              </div>

              <?php  endfor; ?>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
          </div>
      </div>
    <?php endif; ?>

  <!-- Ad Info  10 in case of no image-->
  <?php if(!empty($classified_ad_images)): ?>

  <div class="col-lg-6 ad-info">
<?php else: ?>
    <div class="col-lg-10 ad-info">

<?php endif; ?>
    <div class="row">

      <!-- Ad Title -->
      <div class="col-lg-12 ad-title">
        <div class="row">
          <div class="col-lg-9 col-xs-9">
            <h3><?php echo $title ?></h3>
            <div class="date">  <?php echo arabic_date($created_at) ?></div>
            <div >  <?php echo $views ?> مشاهدة</div>
          </div>
          <div class="col-lg-3 col-xs-3">
            <div class="like-ad <?php if($liked): ?> liked <?php endif; ?>">
              <i class="glyphicon glyphicon-thumbs-up"></i><span><?php echo $likes ?></span></div>
            </div>
          </div>
        </div>

        <!-- Ad description -->
        <div class="col-lg-12">
          <div class="info-title">الوصف</div>
          <div class="info-content description"><?php echo $description ?></div>
        </div>

        <!-- Ad email -->
        <div class="col-lg-6">
          <div class="info-title">البريد الإلكتروني</div>
          <div class="info-content email"><a href="mailto:<?php echo $email ?>"><?php echo $email ?></a></div>
        </div>

        <!-- Ad phone -->
        <div class="col-lg-6">
          <div class="info-title">الهاتف</div>
          <div class="info-content phone"> <a href="tel:<?php echo $phone ?>"><?php echo $phone ?></a></div>
        </div>

      </div>
    </div><!-- /Ad Info -->
    <?php if(isset($fixed_ad_adview)&&!empty($fixed_ad_adview)): ?>
    <?php   $this->load->view("fixed_ads/insideAdview",$fixed_ad_adview); ?>

  <?php endif; ?>

  </div><!-- /Row-->
</div> <!-- /Ad View -->


</div>
</div>

</div><!-- /#page-content-wrapper -->
<?php $this->load->view("footer"); ?>
<script src="<?php echo base_url('assets') ?>/js/vendor/jquery.carousel.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/vendor/jquery.aspect-fill.js"></script>
<script src="<?php echo base_url('assets') ?>/js/vendor/magnific.js"></script>


<script type="text/javascript">

// autoslide

$(function(){
    $('.carousel').carousel({
      interval: 2000
    });
});


// autoslide


var isLogged=false;
<?php if( is_logged()): ?>
isLogged=true;

<?php endif; ?>

(function() {


  if(!isLogged){
      //disable
      $('.glyphicon-thumbs-up').prop("disabled", true);

    }

  //press
  //disab rank-up-btn"
  var classified_ad_id=<?php echo $this->uri->segment(3) ?>;

//like handler
$( ".like-ad" ).click(function() {

  if(isLogged){


    var countHolder = $(this).find("span");
    var count = parseInt(countHolder.text(), 10);
    if($(this).hasClass("liked")) {
      $(this).removeClass("liked");
      countHolder.text(count-1);
    }
    else {
      $(this).addClass("liked");
      countHolder.text(count+1);
    }




    $.ajax({
      url: baseURL+"classified_ads/like",
      data: {classified_ad_id : classified_ad_id},
      type: "post",
      success: function(data) {
          //200
          //
          //$('.glyphicon-thumbs-up').prop("disabled", false);

        },
        error: function(data) {
          //4o4


        },

      });
  }else{
  // alert("Non logged");
  //redirect to login page
  window.location.href = baseURL+'user/login';

  // window.location(baseURL+'user/login')
}

});

//like handler

//delete handler
$( "#delete-link" ).click(function() {

  if(isLogged){

    alertify.confirm("هل أنت متأكد أنك تريد حذف هذا الإعلان؟", function (e) {
      if (e) {
       $.ajax({
        url: baseURL+"classified_ads/delete",
        data: {id :classified_ad_id },
        type: "post",
        success: function(data) {
              //200
              //
              //$('.glyphicon-thumbs-up').prop("disabled", false);

              alertify.alert("تم الحذف بنجاح ");
              window.location="<?php echo base_url() ?>"
            },
            error: function(data) {
              //4o4
              alertify.alert("حدث خطاء في محاولة الحذف .. حاول مرة اخري");
            },

          });
      }
    });



 }else{
  // alert("Non logged");
}

});

//delete handler

})();



</script>
