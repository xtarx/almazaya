<?php   $this->load->view("header"); ?>
<!-- dropzone css -->
<link href="<?php echo base_url(); ?>resouces/css/dropzone.css" type="text/css" rel="stylesheet" />
<!-- dropzone css -->


<!-- Page Content -->
<div id="page-content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <!-- Add Form -->
      <div class="col-lg-9 col-centered">
        <form class="form AdForm" role="form" action="<?= base_url('classified_ads/do_add_mobawab')?>" method="post" enctype="multipart/form-data">

          <div class="row">
            <div class="col-lg-12">
              <div class="form-group">
                <label for="exampleInputTitle">عنوان الإعلان</label>
                <input type="text" class="form-control" id="exampleInputTitle" placeholder="عنوان الإعلان"  name="title" value=""
                data-bv-notempty="true" 
                data-bv-notempty-message="العنوان لا يمكن أن يكون فارغ"
                data-bv-stringlength="true"
                data-bv-stringlength-min="6"
                data-bv-stringlength-max="40"
                data-bv-stringlength-message="يجب أن يكون العنوان أكثر من 8 أحرف وأقل من 40 حرف">
              </div>
            </div>
          </div><!-- Row -->

          <div class="row">
            <div class="col-lg-12">
              <div class="form-group">
        <label for="sub_section">الفئة الفرعية</label>
        <select class="form-control" id="sub_section" name="category_id"
        data-bv-notempty="true" 
        data-bv-notempty-message="الفئة الفرعية لا يمكن أن يكون فارغ">
        <option value="" selected="selected">اختر</option>
      </select>
    </div>
            </div>
          </div><!-- Row -->

    <div class="row">
      <div class="col-lg-6">
        <div class="form-group">
          <label for="exampleInputEmail">البريد الإلكتروني</label>
          <input type="email" class="form-control" id="exampleInputEmail" placeholder="البريد الإلكتروني" name="email" value="<?= $this->session->userdata['email']  ?>"
          data-bv-notempty="true" 
          data-bv-notempty-message="البريد الإلكتروني لا يمكن أن يكون فارغ"
          data-bv-emailaddress="true"
          data-bv-emailaddress-message="البريد الإلكتروني غير صالح">
        </div>
      </div>
      <div class="col-lg-6">    
        <div class="form-group">
          <label for="exampleInputPhone">رقم الهاتف</label>
          <input type="text" class="form-control" id="exampleInputPhone" placeholder="رقم الهاتف" name="phone" value="<?=  $this->session->userdata['phone'] ?>"
          data-bv-notempty="true" 
          data-bv-notempty-message="رقم الهاتف لا يمكن أن يكون فارغ"
          data-bv-numeric="true" 
          data-bv-numeric-message="رقم الهاتف يحتوي علي ارقام فقط">
        </div>
      </div>
    </div><!-- Row -->

    <div class="row">
      <div class="col-lg-12">
        <div class="form-group">
          <label for="exampleInputDescr">الوصف</label>
          <textarea class="form-control" id="exampleInputDescr"  name="description" rows="3"
          data-bv-notempty="true" 
          data-bv-notempty-message="الوصف لا يمكن أن يكون فارغ"
          data-bv-stringlength="true"
          data-bv-stringlength-max="500"
          data-bv-stringlength-message="يجب أن يكون الوصف أقل من 500 حرف"></textarea>
        </div>
      </div>
    </div><!-- Row -->


<button type="submit" class="btn wide-btn btn-danger btn-lrg">أضف الإعلان</button>
</form>

</div><!-- Add Form -->
</div><!-- /row -->
</div><!-- /container-fluid -->
</div><!-- /#page-content-wrapper -->



<?php   $this->load->view("footer"); ?>

<!-- multiselect bootstrap -->

<!-- Include the plugin's CSS and JS: -->
<script type="text/javascript" src="<?php echo base_url('assets') ?>/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="<?php echo base_url('assets') ?>/css/bootstrap-multiselect.css" type="text/css"/>

<!-- Initialize the plugin: -->
<script type="text/javascript">
$(document).ready(function() {
  $('.multiselect').multiselect();
    // $('#sel_section').multiselect();
    // $('#sub_section').multiselect();

  });
</script>

<script type="text/javascript">
(function() {



   $.ajax({
    url: "<?php echo base_url('classified_ads/get_categories/'); ?>/"+"19",
    type: "GET",
    success: function(data) {
     $('#sub_section').html(data);



   },
 });
   



 


  $( "#sel_section" ).change(function() {

    if( $( "#sel_section" ).val() != null){
     $.ajax({
      url: "<?php echo base_url('classified_ads/get_categories/'); ?>/"+ $( "#sel_section" ).val(),
      type: "GET",
      success: function(data) {
            // $('#sub_section').prop("disabled", false);

            $('#sub_section').html(data);

            // $('#sub_section').multiselect('rebuild');

            // var data = [
            // {label: "ACNP", value: "ACNP"},
            // {label: "test", value: "test"}
            // ];
            // $("#sub_section").multiselect('dataprovider', data);


          },
        });
   }

 });

})();

//validation

</script>

<!-- multiselect bootstrap -->
