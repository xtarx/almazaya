<?php  $this->load->view("admin/admin_header"); ?>


<!-- Groc stuff -->
<?php if(isset($css_files)): ?>

  <?php 
  foreach($css_files as $file): ?>
  <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>

<?php foreach($js_files as $file): ?>
  <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<?php endif; ?>
<link type="text/css" rel="stylesheet" href="<?= base_url(); ?>assets/css/groc_rtl.css" />

<!-- Groc stuff -->

<link rel="stylesheet" href="<?php echo base_url('assets') ?>/css/carousel.css">
<link rel="stylesheet" href="<?php echo base_url('assets') ?>/css/magnific-popup.css">

<!-- Page Content -->
<div id="page-content-wrapper">
  <div class="container-fluid">
    <div class="row">

     <!-- Filtering & Search -->
     <div class="filter-header col-lg-12">


      <div class="row">
        <div class="tags col-lg-9">
          <a href="<?= base_url('admin/')?>"><span class="tag">     لوحة التحكم</span></a>
          <i>></i>
          <a href="<?php echo base_url("admin/".$bread_link ); ?>"> <span class="tag"> <?php  if(isset($bread)) echo $bread ?></span></a>
        </div>
      </div>

      <!-- msg view -->
      <?php if(isset($msg)): ?>
      <span> <?php echo $msg;  ?> </span>
    <?php endif; ?> 
    <!-- msg view -->
    <div class="separator"></div>







    <?php echo $output; ?>


  </div>
</div>

</div><!-- /#page-content-wrapper -->
<?php $this->load->view("admin/admin_footer"); ?>
