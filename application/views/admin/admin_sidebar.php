
<!-- Sidebar -->
<div id="sidebar-wrapper" class="nice-scroll">
    <div class="sidebar-nav">

      <!-- Main Categories -->
      <ul class="main-categories">
        <li class="sidebar-brand"><a href="<?php echo base_url()   ?>"><img src="<?php echo base_url()   ?>/assets/img/logo.png"/></a></li>
      
         <!-- add an add  -->
        <div class="sidebar-buttons">
          
          <!-- add an add  -->
          <li class="sidebar-button">
            <a href="<?php echo base_url('') ?>" class="btn btn-lg btn-danger">العودة للموقع</a>
          </li>
          <!-- end add an add  -->
        </div>


      <li class="separator"></li>

      <li>
        <a href="#"><span class="glyphicon glyphicon-globe"></span>ادارة المستخدمين</a>
        <ul class="sub-list">
          <li  <?php if($bread_link=="users"): ?>class="active" <?php endif; ?> ><a href="<?= base_url('admin/users')?>">المستخدمين</a></li>
        </ul>
      </li>


    

      <li>
        <a href="#"><span class="glyphicon glyphicon-globe"></span>ادارة الاعلانات المبوبة</a>
        <ul class="sub-list">
          <li  <?php if($bread_link=="issues"): ?>class="active" <?php endif; ?> ><a href="<?= base_url('admin/issues')?>">الاعداد</a></li>
          <li  <?php if($bread_link=="sections"): ?>class="active" <?php endif; ?> ><a href="<?= base_url('admin/sections')?>">الأقسام</a></li>
          <li  <?php if($bread_link=="categories"): ?>class="active" <?php endif; ?> ><a href="<?= base_url('admin/categories')?>">الفئات</a></li>
          <li   ><a href="<?= base_url('classified_ads/add')?>">اضافة الاعلانات </a></li>
          <li   ><a href="<?= base_url('classified_ads/add_mobawab')?>">اضافة الاعلانات المبوبة</a></li>

          <li  <?php if($bread_link=="classified_ads"): ?>class="active" <?php endif; ?> ><a href="<?= base_url('admin/classified_ads')?>">ادارة الاعلانات </a></li>

          <li  <?php if($bread_link=="mobawaba_ads"): ?>class="active" <?php endif; ?> ><a href="<?= base_url('admin/mobawaba_ads')?>">ادارة الاعلانات المبوبة</a></li>


        </ul>
      </li>


      <li>
        <a href="#"><span class="glyphicon glyphicon-globe"></span>اادارة لاعلانات الثابتة</a>
        <ul class="sub-list">
          <li  <?php if($bread_link=="advertisements"): ?>class="active" <?php endif; ?> ><a href="<?= base_url('admin/advertisements')?>">الاعلانات الثابتة</a></li>
          <li  <?php if($bread_link=="locations"): ?>class="active" <?php endif; ?> ><a href="<?= base_url('admin/locations')?>">الاماكن</a></li>

        </ul>
      </li>


      
      
  </ul><!-- /Main Categories -->
 
</div>
</div><!-- /#sidebar-wrapper -->
