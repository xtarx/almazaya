<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html dir="rtl" lang="ar" class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>المزاد  <?php if (isset($page_title)) {?>| <?php echo substr($page_title,0,100);   }?></title>
  <meta name="description" content="Mazaad | E-Commerce Site">
  <meta name="author" content="Karim Tarek">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<?php echo base_url('assets') ?>/img/favicon.ico" type="image/x-icon">
  <link rel="icon" href="<?php echo base_url('assets') ?>/img/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="<?php echo base_url('assets') ?>/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets') ?>/css/bootstrap-rtl.min.css">
  <!-- Custom CSS -->
  <link href='http://fonts.googleapis.com/earlyaccess/droidarabickufi.css' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="<?php echo base_url('assets') ?>/css/main.css">
  <script src="<?php echo base_url('assets') ?>/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body>
    <!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div id="wrapper">

      <!-- Navigation bar -->
      <div class="navbar navbar-default navbar-fixed-top" role="navigation">
          <div class="nav-header">
            <button type="button" class="navbar-toggle menu-toggle pull-right" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>

          <div class="dropdown-actions">
          <?php if(is_logged()): ?>

          <!-- logged in user actions -->

          <div class="dropdown user-actions">
            <div class=" dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown">
              <div class="dropdown-title"><span class="glyphicon glyphicon-chevron-down"></span><?php echo $this->session->userdata['name'] ?></div>
              <!-- check if image is set -->
              <?php  
              $session_img=$this->session->userdata['user_image'];
              if(!isset($session_img) || ($session_img=='')){$user_image="default_avatar.png";}else{$user_image=$session_img;}
              ?>
              <!-- check if image is set -->

              <img class="avatar" src="<?php  echo $user_image?>"/>
            </div>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
              <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('user/edit') ?>">تعديل الحساب</a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('classified_ads/my_likes') ?>">مفضلتي</a></li>
              <li role="presentation" class="divider"></li>

              <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('user/logout') ?>">تسجيل الخروج</a></li>
            </ul>
          </div>
          <!-- end logged in user  actions-->
        <? else: ?>


        <!-- NON-logged in user actions -->

        <div class="dropdown user-actions">
          <div class=" dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown">
            <div class="dropdown-title"><span class="glyphicon glyphicon-chevron-down"></span>الدخول</div>
          </div>
          <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
            <li role="presentation"><a class="toggle-modal" data-target="#login-form" data-tab="signIn-form" role="menuitem" tabindex="-1" href="#">تسجيل الدخول</a></li>
            <li role="presentation"><a class="toggle-modal" data-target="#login-form" data-tab="signUp-form" role="menuitem" tabindex="-1" href="#">التسجيل</a></li>
          </ul>
        </div>
        <!-- end NON-logged in user  actions-->
      <?php endif; ?>


    </div>
  </div><!-- /Navigation bar -->

  <?php   $this->load->view("admin/admin_sidebar"); ?>
