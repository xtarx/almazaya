
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span2">
      <div class="well sidebar-nav">
        <ul class="nav nav-list">

          <li class="nav-header">ادارة المستخدمين</li>
          <li><a class="active" href="<?= base_url('admin/users')?>">المستخدمين</a></li>

          <li class="nav-header">ادارة الاعلانات المبوبة</li>
          <li><a class="" href="<?= base_url('admin/issues')?>">الاعداد</a></li>
          <li><a class="" href="<?= base_url('admin/sections')?>">الأقسام</a></li>

          <li><a class="" href="<?= base_url('admin/categories')?>">الفئات</a></li>
          <li style="background-color: yellow;"><a class="" href="<?= base_url('classified_ads/add')?>">اضافة الاعلانات المبوبة</a></li>

          <li><a class="" href="<?= base_url('admin/classified_ads')?>">ادارة الاعلانات المبوبة</a></li>

          <li class="nav-header">اادارة لاعلانات الثابتة</li>
          <li><a class="" href="<?= base_url('admin/advertisements')?>">الاعلانات الثابتة</a></li>
          <li><a class="" href="<?= base_url('admin/locations')?>">الاماكن</a></li>





        </ul>
      </div><!--/.well -->
</div><!--/span-->