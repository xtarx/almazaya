
<!-- Sidebar -->
<div id="sidebar-wrapper" class="nice-scroll">
  <div class="sidebar-nav">

    <!-- Main Categories -->
    <ul class="main-categories">
      <li class="sidebar-brand"><a href="<?php echo base_url()   ?>"><img src="<?php echo base_url()   ?>/assets/img/logo.png"/></a><li class="sidebar-button phone-number">
        50400334<i class="glyphicon glyphicon-earphone"></i>
      </li></li>
      
      <?php  if(is_logged()): 
          //if admin
  if(is_admin($this->session->userdata('user_id'))):?>

     <!-- add an add  -->
     <div class="sidebar-buttons">
    
      <li class="sidebar-button">
        <a href="<?php echo base_url('classified_ads/add/') ?>" class="btn btn-lg btn-danger">أضف إعلان</a>
      </li>
      <!-- end add an add  -->
      <!-- add an add  -->
      <li class="sidebar-button">
        <a href="<?php echo base_url('admin') ?>" class="btn btn-lg btn-danger">لوحة التحكم</a>
      </li>
      <!-- end add an add  -->
    </div>


  <?php  else:  ?>
  <!-- non admin actions -->

  <!-- add an add  -->
  <div class="sidebar-buttons">
    <li class="sidebar-button">
      <a href="<?php echo base_url('classified_ads/add_mobawab/') ?>" class="btn btn-lg btn-danger">أضف إعلان مبوب</a>
    </li>
    
  </div>
    <!-- end add an add  -->



  <!-- non admin actions -->


<?php  endif;  ?>

<?php  else:  ?>
<!-- non logged in actions -->

  <div class="row stores-buttons">
    <a class="col-xs-6" href="https://itunes.apple.com/us/app/almazad-magazine-q8/id725336440?ls=1&mt=8" ><img  src="<?php echo base_url()   ?>/assets/img/appstore.png"/></a>
    <a class="col-xs-6" href="https://play.google.com/store/apps/details?id=com.mazad.app" ><img  src="<?php echo base_url()   ?>/assets/img/GooglePlay.png"/></a>
  </div>  



<!-- add an add  -->
  <div class="sidebar-buttons">
    <li class="sidebar-button">
      <a href="<?php echo base_url('classified_ads/add_mobawab/') ?>" class="btn btn-lg btn-danger">أضف إعلان مبوب</a>
    </li>  
  </div>
    <!-- end add an add  -->

<?php  endif;  ?>

<li class="separator"></li>
<?php 
        //if no category selected
$is_category_selected=false;
if(isset($selected_category_id)){
  $is_category_selected=true;
}
?>


<?php foreach($sections as $section) :?>
  <li  <?php if(!$is_category_selected&&isset($selected_section_name)&&$selected_section_name==$section['name']): ?>class="active" <?php endif; ?>  >
    <a href="<?php echo base_url("classified_ads/list_ads/".$section['url']); ?>"><span class="glyphicon glyphicon-globe"></span> <?php echo $section['name'] ?></a>
    <!-- Sub Categories -->
    <?php if(isset($selected_section_name)&&$selected_section_name==$section['name']): ?>
    <ul class="sub-categories">
      <?php foreach($categories as $category) :?>
      <li <?php if($is_category_selected&&isset($selected_category_name)&&$selected_category_name==$category['name']): ?>class="active" <?php endif; ?>>
        <a href="<?php echo  base_url('classified_ads/list_ads/'.$this->uri->segment(3).'/'    .$category['id'] )?>"><span class="ball red"></span><?php echo $category['name'] ?></a>
      </li>
      <?php endforeach; ?>
    </ul>
<?php  endif;  ?>


  </li>

<?php endforeach; ?>




</ul><!-- /Main Categories -->

<?php if ( isset($categories) &&$this->uri->segment(3)!=null ) : ?>
<!--   <div class="separator"></div> -->



<!-- /Sub Categories -->

<?php  if(is_mobawaba()):?>

  <!-- footer warning  -->
  <ul class="sidebar-footer">
    <li class="text-center">الإعلانات الموجودة علي مسئولية المعلن</li> 
  </ul>
  <!-- footer warning  -->
<?php endif; ?>
<?php endif; ?>
</div>
</div><!-- /#sidebar-wrapper -->