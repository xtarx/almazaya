<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<?php 
	foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
body
{
	font-family: Arial;
	font-size: 14px;
}
a {
	color: blue;
	text-decoration: none;
	font-size: 14px;
}
a:hover
{
	text-decoration: underline;
}
</style>
</head>
<body>
	<div>
		<a href='<?php echo site_url('admin/users')?>'>users</a> |
		<a href='<?php echo site_url('admin/sections')?>'>sections</a> |
		<a href='<?php echo site_url('admin/categories')?>'>categories</a> | 
		<a href='<?php echo site_url('admin/classified_ads')?>'>classified_ads</a> |

		<a href='<?php echo site_url('admin/advertisements')?>'>advertisements</a> |		 
		<a href='<?php echo site_url('admin/advertisement_locations')?>'>advertisement_locations</a> | 
		<a href='<?php echo site_url('admin/super_user')?>'>super user</a> | 

	</div>
	<div style='height:20px;'></div>  
	<div>
		<?php echo $output; ?>
	</div>
</body>
</html>
