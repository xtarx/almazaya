<?php   $this->load->view("user/header"); ?>
<link rel="stylesheet" href="<?php echo base_url('assets') ?>/css/registration.css">

<div id="wrapper">

  <!-- Login & Register Forms -->
  <div id="login-form" class="col-centered col-sm-4 login-form">
    <!-- Login & Register Tabs -->
    <div class="login-header row">
     <span class="tab col-xs-6 selected" data-toggle="signIn-form">تعديل الحساب</span>
   </div>


   <!-- Register Form -->
   <form class="form signUp-form row " role="form" method="post"  action="<?= base_url("user/do_change_password")?>"  >
   
<div class="col-lg-12">

  <!-- password -->
  <div class="form-group inner-addon right-addon">
    <label class="sr-only" for="exampleInputPass">كلمة المرور</label>
    <i class="glyphicon glyphicon-lock"></i>
    <input type="password" class="form-control" id="exampleInputPass" placeholder="كلمة المرور" name="password"

    data-bv-stringlength="true"
    data-bv-stringlength-min="3"
    data-bv-stringlength-message="يجب أن يكون كلمة المرور أقل من 30 حرف"
    data-bv-identical-field="password_confirm"
    data-bv-identical-message ="يجب أن تتطابق كلمات المرور"


    >
  </div>
  <!-- password -->

  <!-- confirm_password -->
  <div class="form-group inner-addon right-addon">
    <label class="sr-only" for="exampleInputPass">تآكيد كلمة المرور</label>
    <i class="glyphicon glyphicon-lock"></i>
    <input type="password" class="form-control"  placeholder="تآكيد  كلمة المرور" name="password_confirm"

    data-bv-stringlength="true"
    data-bv-stringlength-max="30"
    data-bv-stringlength-message="يجب أن يكون كلمة المرور أقل من 30 حرف"
    data-bv-identical-field="password"
    data-bv-identical-message ="يجب أن تتطابق كلمات المرور"
    >
  </div>
  <!-- confirm_password -->


  <button type="submit" class="btn wide-btn btn-primary btn-lrg">تعديل حساب</button>
</div>

</form>
<!-- Register Form -->



</div>

</div><!-- /#wrapper -->

<?php   $this->load->view("user/footer"); ?>


<script src="<?php echo base_url(); ?>resouces/dropzone.min.js"></script>

<script src="<?php echo base_url('assets') ?>/js/registration.js"></script>
