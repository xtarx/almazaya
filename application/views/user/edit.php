<?php   $this->load->view("user/header"); ?>
<link rel="stylesheet" href="<?php echo base_url('assets') ?>/css/registration.css">

<div id="wrapper">

  <!-- Login & Register Forms -->
  <div id="login-form" class="col-centered col-sm-4 login-form">
    <!-- Login & Register Tabs -->
    <div class="login-header row">
     <span class="tab col-xs-6 selected" data-toggle="signIn-form">تعديل الحساب</span>
   </div>


   <!-- Register Form -->
   <form class="form signUp-form row " role="form" method="post"  action="<?= base_url("user/do_edit")?>"  enctype="multipart/form-data">
    <div class="col-lg-7">
      <div class="form-group inner-addon right-addon">
        <label class="sr-only" for="exampleInputUser">اسم المستخدم</label>
        <i class="glyphicon glyphicon-user"></i>
        <input type="text" class="form-control" id="exampleInputUser" placeholder="اسم المستخدم" name="name"
        value="<?=$name?>"
        data-bv-notempty="true" 
        data-bv-notempty-message="اسم المستخدم لا يمكن أن يكون فارغ"
        data-bv-stringlength="true"
        data-bv-stringlength-max="30"
        data-bv-stringlength-message="يجب أن يكون اسم المستخدم أقل من 30 حرف"

        >
      </div>
      <div class="form-group inner-addon right-addon">
        <label class="sr-only" for="exampleInputEmail">البريد الإلكتروني</label>
        <i class="glyphicon glyphicon-envelope"></i>
        <input type="email" class="form-control" id="exampleInputEmail" placeholder="البريد الإلكتروني" name="email"
        value="<?=$email ?>"
        data-bv-notempty="true" 
        data-bv-notempty-message="البريد الإلكتروني لا يمكن أن يكون فارغ"
        data-bv-emailaddress="true"
        data-bv-emailaddress-message="البريد الإلكتروني غير صالح"

        >
      </div>
    </div>
<?php if(!isset($this->session->userdata['provider']) ): ?>
<!-- image upload -->
    <div class="col-lg-5" >
      <div id="dropzone">
        <?php if(isset($image) &&$image!=null): ?>
        <img src="<?php echo base_url() ?>uploads/users/images/<?php  echo $image?>"> </img>
      <?php  else:?>

      <div class="dropzone_NOT">ضع صورتك هنا</div>
    <?php  endif;?>

    <input type="file"  name="userfile" accept="image/png, image/jpeg, image/jpg" />            

  </div>
</div>
<!-- image upload -->
<?php endif; ?>
<div class="col-lg-12">
  <?php if(!isset($this->session->userdata['provider']) ): ?>

  <!-- password -->
  <div class="form-group inner-addon right-addon">
    <label class="sr-only" for="exampleInputPass">كلمة المرور</label>
    <i class="glyphicon glyphicon-lock"></i>
    <input type="password" class="form-control" id="exampleInputPass" placeholder="كلمة المرور" name="password"

    data-bv-stringlength="true"
    data-bv-stringlength-max="30"
    data-bv-stringlength-message="يجب أن يكون كلمة المرور أقل من 30 حرف"

    >
  </div>
  <!-- password -->
<?php endif; ?>
  <div class="form-group inner-addon right-addon">
    <label class="sr-only" for="exampleInputPass">رقم الهاتف</label>
    <i class="glyphicon glyphicon-earphone"></i>
    <input type="text" class="form-control" id="exampleInputPhone" placeholder="رقم الهاتف" name="phone"
    value="<?=$phone ?>"
    data-bv-notempty="true" 
    data-bv-notempty-message="رقم الهاتف لا يمكن أن يكون فارغ"

    data-bv-numeric="true" 
    data-bv-numeric-message="رقم الهاتف يحتوي علي ارقام فقط"

    >
  </div>
  <button type="submit" class="btn wide-btn btn-primary btn-lrg">تعديل حساب</button>
</div>

</form>
<!-- Register Form -->



</div>

</div><!-- /#wrapper -->

<?php   $this->load->view("user/footer"); ?>


<script src="<?php echo base_url(); ?>resouces/dropzone.min.js"></script>

<script src="<?php echo base_url('assets') ?>/js/registration.js"></script>
