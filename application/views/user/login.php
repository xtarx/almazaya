<?php   $this->load->view("user/header"); ?>

<div id="wrapper">


  <!-- Login & Register Forms -->
  <div id="login-form" class="col-centered col-sm-4 login-form">
    <!-- Login & Register Tabs -->
    <div class="login-header row">
     <span class="tab col-xs-6 selected" data-toggle="signIn-form">الدخول</span>
     <a href="<?php echo base_url("user/register") ?>"><span class="tab col-xs-6" data-toggle="signUp-form">حساب جديد</span></a>
   </div>

   <!-- Login Form -->
   <form class="form signIn-form" action="<?= base_url("user/do_login")?>"  method="post"  role="form">
     <!-- social buttons -->
  <?php   $this->load->view("account/social_buttons"); ?>
  <!-- social buttons -->



    <?php if (isset($error)): ?>
    <div class="red"  >
     <?php echo $error; ?>
   </div>
 <?php endif; ?>

 <input type="hidden" name="redirection" value="<?php if(isset($redirection )){echo $redirection; } ?>" />

 <div class="form-group inner-addon right-addon">
  <label class="sr-only" for="exampleInputEmail">البريد الإلكتروني</label>
  <i class="glyphicon glyphicon-envelope"></i>
  <input type="email" class="form-control" id="exampleInputEmail" placeholder="البريد الإلكتروني" name="email"
  value="<?php echo $this->input->post('email'); ?>" 

  data-bv-notempty="true" 
  data-bv-notempty-message="البريد الإلكتروني لا يمكن أن يكون فارغ"
  data-bv-emailaddress="true"
  data-bv-emailaddress-message="البريد الإلكتروني غير صالح"
  >
</div>

<div class="form-group inner-addon right-addon">
  <label class="sr-only" for="exampleInputPass">كلمة المرور</label>
  <i class="glyphicon glyphicon-lock"></i>
  <input type="password" class="form-control" id="exampleInputPass" placeholder="كلمة المرور" name="password" >
</div>

<button type="submit" class="btn wide-btn btn-primary btn-lrg">تسجيل الدخول</button>
</form>
<!-- Login Form -->







</div>

</div><!-- /#wrapper -->

<?php   $this->load->view("user/footer"); ?>