  <div class="backdrop"></div>

    <script src="<?php echo base_url('assets') ?>/js/vendor/jquery-1.11.0.min.js"></script>
    <script src="<?php echo base_url('assets') ?>/js/vendor/jquery.nicescroll.min.js"></script>
    <script src="<?php echo base_url('assets') ?>/js/vendor/jquery.dropdowns.min.js"></script>
   	<script src="<?php echo base_url('assets') ?>/js/bootstrapValidator.min.js"></script>
    <script src="<?php echo base_url('assets') ?>/js/main.js"></script>
    
  </body>
</html>