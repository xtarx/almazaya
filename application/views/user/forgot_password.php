<?php   $this->load->view("user/header"); ?>
<link rel="stylesheet" href="<?php echo base_url('assets') ?>/css/registration.css">

<div id="wrapper">

  <!-- Login & Register Forms -->
  <div id="login-form" class="col-centered col-sm-4 login-form">
    <!-- Login & Register Tabs -->
    <div class="login-header row">
     <span class="tab col-xs-6 selected" data-toggle="signIn-form">استرجاع كلمة المرور</span>
   </div>


   <!-- Register Form -->
   <form class="form signUp-form row " role="form" method="post"  action="<?= base_url('user/do_forgot')?>" >

    <!-- erros div   -->
    <?php if ((validation_errors())): ?>
    <div class="red"  >
     <?php echo validation_errors(); ?>
   </div>
 <?php endif; ?>

   <?php if (isset($error)): ?>
    <div class="red"  >
     <?php echo $error; ?>
   </div>
 <?php endif; ?>


 <!-- erros div   -->



 <div class="col-lg-7">

  <div class="form-group inner-addon right-addon">
    <label class="sr-only" for="exampleInputEmail">البريد الإلكتروني</label>
    <i class="glyphicon glyphicon-envelope"></i>
    <input type="email" class="form-control" id="exampleInputEmail" placeholder="البريد الإلكتروني" name="email"
    data-bv-notempty="true" 
    data-bv-notempty-message="البريد الإلكتروني لا يمكن أن يكون فارغ"
    data-bv-emailaddress="true"
    data-bv-emailaddress-message="البريد الإلكتروني غير صالح"
    >
  </div>
    <button type="submit" class="btn wide-btn btn-primary btn-lrg">استرجاع</button>

</div>



</form>
<!-- Register Form -->



</div>

</div><!-- /#wrapper -->

<?php   $this->load->view("user/footer"); ?>



<script src="<?php echo base_url('assets') ?>/js/registration.js"></script>
