<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html dir="rtl" lang="ar" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>المزاد  <?php if (isset($page_title)) {?>| <?php echo substr($page_title,0,100);   }?></title>
    <meta name="description" content="Mazaad | E-Commerce Site">
    <meta name="author" content="Karim Tarek">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url('assets') ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets') ?>/css/bootstrap-rtl.min.css">
    <!-- Custom CSS -->
    <link href='http://fonts.googleapis.com/earlyaccess/droidarabickufi.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url('assets') ?>/css/main.css">
    <script src="<?php echo base_url('assets') ?>/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
  <body class="login-page" style="background: url(<?php echo base_url('assets') ?>/img/login-lander.jpg) no-repeat center center fixed;">
    <!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
