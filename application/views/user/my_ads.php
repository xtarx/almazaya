<?php   $this->load->view("header"); ?>
<!-- Page Content -->
<div id="page-content-wrapper">
  <div class="container-fluid">
    <div class="row">

      <!-- Filtering & Search -->
      <div class="filter-header col-lg-12">
        <h2 class="category-title">مفضلتي
      </h2>

      <div class="separator"></div>
      
     
    </div>

    <!-- Ads List -->
    <div class="AdsList margin-top col-lg-12" id="classifieds_list">
      <!-- Row -->
      <div class="row" id="ads-row">
        <?php foreach ($ads as $value): ?>

        <!-- Ad Card -->
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 item-wrapper">
          <div class="ad-element">
           <a href="<?php echo base_url('classified_ads/view/'.$value['id']) ?>" >
            <div class="ad-photo" style="background-image: url('<? echo base_url('uploads/classified_ads/images/'. $value['image']) ?>');">
              <div class="overlay"><span class="glyphicon glyphicon-eye-open"></span></div>
            </div>
          </a>
          <div class="ad-details row">
            <div class="col-xs-12">
              <a href="<?php echo base_url('classified_ads/view/'.$value['id']) ?>" class="ad-title"><?php   echo mb_substr($value['title'], 0, 50); ?></a>
              <div class="ad-owner"><?php echo mb_substr($value['description'], 0, 50) ?>..</div>
            </div>
            <!-- <div class="ad-price col-xs-4">7.4 $</div> -->
            </div>
            <div class="separator"></div>
            <div class="ad-details row">
              <div class="col-xs-6"><span class="glyphicon glyphicon-thumbs-up"></span><?php echo $value['likes'] ?></div>
           </div>
         </div>
       </div>

    <?php endforeach; ?>



 </div><!-- /Row-->
</div> <!-- /Ads List -->

<!-- Spinner Loader -->
<div class="spinner" style=" display: none; ">
  <div class="bounce3"></div>
  <div class="bounce2"></div>
  <div class="bounce1"></div>
</div>

</div>
</div>

</div><!-- /#page-content-wrapper -->


<?php   $this->load->view("footer"); ?>
<script src="<?php echo base_url('assets') ?>/js/vendor/isotope.pkgd.min.js"></script>
<!-- load infite scroll -->    
<!-- a<script src="<?= base_url(); ?>assets/js/scroll.js"></script> -->
<!-- load infite scroll -->
