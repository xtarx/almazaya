<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * @author XtarX
 * @author ahmedmahmoudit
 * @package helper.common
 */

if (!function_exists('generate_token')) {
    if (!function_exists('generate_token')) {
        /**
         * This method is used to Generate Token.
         * @method generate_token
         * @return {String} Generated token.
         *
         */
        function generate_token()
        {
            $token = md5(uniqid() . microtime() . rand());
            return $token;
        }
    }

    if (!function_exists('redirect_home')) {

        function redirect_home()
        {

            redirect('classified_ads/list_ads/');

        }
    }

    if (!function_exists('sendmail')) {

        function sendmail($to,$subject,$message)
        {
            $CI = get_instance();

            $CI->load->library('email');
            $CI->email->from(FROM_MAIL, FROM_NAME);
            $CI->email->to($to); 
            $CI->email->subject($subject);
            $CI->email->message($message);  
            $CI->email->send();

        }
    }


    if (!function_exists('is_logged')) {

        function is_logged() {
            $CI = get_instance();
            

            $session_user = $CI->session->userdata('user_id');

            if(!isset($session_user) || ($session_user==''))
            {
                return false;
            }
            return true;
        }
    }
    

    if (!function_exists('is_mobawaba')) {
        function is_mobawaba() {
            $CI = get_instance();    
            return  $CI->uri->segment(3)=="mobawab";
        }
    }

    if (!function_exists('justify_string')) {
        function justify_string($str,$max=30) {        
            $char_count=mb_strlen($str);
            if($char_count>$max){
                return mb_substr($str, 0, $max) . ".."; 
            }
            return $str; 

        }
    }

    if (!function_exists('is_admin')) {

        function is_admin($id) {
            $CI = get_instance();
            $CI->load->model('users_model');
            // $session_user = $CI->session->userdata('is_admin');

            // if(!isset($session_user) || ($session_user==''))
            // {
            //     return false;
            // }
            // return true;
            $response=$CI->users_model->get_by(array('id' => $id,'role' => 1 ));

            if($response){
                return true;
            }
            return false;
        }
    }
    
    function arabic_date($your_date)
    {

        $time = strtotime($your_date);

        $your_date = date('Y-m-d',$time);


        $months    = array(
            "Jan" => "يناير",
            "Feb" => "فبراير",
            "Mar" => "مارس",
            "Apr" => "أبريل",
            "May" => "مايو",
            "Jun" => "يونيو",
            "Jul" => "يوليو",
            "Aug" => "أغسطس",
            "Sep" => "سبتمبر",
            "Oct" => "أكتوبر",
            "Nov" => "نوفمبر",
            "Dec" => "ديسمبر"
            );
        $en_month  = date("M", strtotime($your_date));
        foreach ($months as $en => $ar) {
            if ($en == $en_month) {
                $ar_month = $ar;
            }
        }
        
        $find          = array(
            "Sat",
            "Sun",
            "Mon",
            "Tue",
            "Wed",
            "Thu",
            "Fri"
            );
        $replace       = array(
            "السبت",
            "الأحد",
            "الإثنين",
            "الثلاثاء",
            "الأربعاء",
            "الخميس",
            "الجمعة"
            );
        $ar_day_format = date('D');
        $ar_day        = str_replace($find, $replace, $ar_day_format);
        $standard               = array(
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"
            );
        $eastern_arabic_symbols = array(
            "٠",
            "١",
            "٢",
            "٣",
            "٤",
            "٥",
            "٦",
            "٧",
            "٨",
            "٩"
        );                      //02، يوليو 2014
        $current_date           =  date('d') . ' , ' . $ar_month . '  ' . date('Y');
        $arabic_date            = str_replace($standard, $eastern_arabic_symbols, $current_date);
        return $current_date;
    }
    
}

?>