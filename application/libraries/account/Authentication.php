<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authentication {

	var $CI;

	/**
	 * Constructor
	 */
	function __construct()
	{
		// Obtain a reference to the ci super object
		$this->CI =& get_instance();

		$this->CI->load->library('session');
	}

	// --------------------------------------------------------------------

	/**
	 * Check user signin status
	 *
	 * @access public
	 * @return bool
	 */
	function is_signed_in()
	{
		return $this->CI->session->userdata('account_id') ? TRUE : FALSE;
	}

	// --------------------------------------------------------------------

	/**
	 * Sign user in
	 *
	 * @access public
	 * @param int  $account_id
	 * @param bool $remember
	 * @return void
	 */
	function sign_in($account_id,$info,$provider )
	{

//handling image and defaulta vatar

		$image="default_avatar.png";
		$image_url=base_url() .'uploads/users/images/' .$image;
		$this->CI->load->model('users_model');
		$response=$this->CI->users_model->get_by(array('id' => $account_id,'enabled' => 1));


		switch ($provider) {
			case 'twitter':
			if($info['profile_image_url']){
				$image_url_twitter=$info['profile_image_url_https'];


				$headers = get_headers($image_url_twitter,1);
				if(isset($headers['Location'])) {
					$image_url = $headers['Location'];
				} 


			}
			$session_data = array(
				'user_id'  =>$account_id,
				'name'     =>$response['name'],
				'email'     => $response['email'],
				'phone'     => $response['phone'],
				'user_image'     => $image_url,
				'provider'     => $provider,
				);

			break;
			
			case 'facebook':

			$image_url_fb='http://graph.facebook.com/'.$info['id'].'/picture';

			$headers = get_headers($image_url_fb,1);
			if(isset($headers['Location'])) {
				$image_url = $headers['Location'];
			} 

    $session_data = array(
    	'user_id'  =>$account_id,
    	'name'     =>$response['name'],
    	'email'     => $response['email'],
    	'phone'     => $response['phone'],
    	'user_image'     => $image_url,
    	'provider'     => $provider,
    	);

    break;
}

$this->CI->session->set_userdata($session_data);

		// print_r($this->CI->session->all_userdata());
		// echo "herefb";
		// exit();

$this->CI->load->model('users_model');

$this->CI->users_model->update_last_signed_in_datetime($account_id);

		// Redirect signed in user with session redirect
if ($redirect = $this->CI->session->userdata('sign_in_redirect'))
{
	$this->CI->session->unset_userdata('sign_in_redirect');
	redirect($redirect);
}
		// Redirect signed in user with GET continue
elseif ($this->CI->input->get('continue'))
{
	redirect($this->CI->input->get('continue'));
}

redirect('');
}

	// --------------------------------------------------------------------

	/**
	 * Sign user out
	 *
	 * @access public
	 * @return void
	 */
	function sign_out()
	{
		$this->CI->session->unset_userdata('account_id');
	}

	// --------------------------------------------------------------------

	/**
	 * Check password validity
	 *
	 * @access public
	 * @param object $account
	 * @param string $password
	 * @return bool
	 */
	function check_password($password_hash, $password)
	{
		$this->CI->load->helper('account/phpass');

		$hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);

		return $hasher->CheckPassword($password, $password_hash) ? TRUE : FALSE;
	}

}


/* End of file Authentication.php */
/* Location: ./application/account/libraries/Authentication.php */