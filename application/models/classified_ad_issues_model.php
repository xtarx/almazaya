<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class classified_ad_issues_model extends MY_Model {
	
	protected $_table = 'classified_ad_issues';
	protected $return_type = 'array';

	public function get_selected_issues($id)
	{

		$array=array();
		$issues=$this->classified_ad_issues_model->get_many_by( array('classified_ad_id' => $id, ));

		foreach ($issues as $key => $issue) {
			$array[]=$issue['issue_id'];
		}

		return $array;
	}
	

}