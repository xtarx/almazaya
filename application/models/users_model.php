<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class users_model extends MY_Model {


	protected $_table = 'users';
//	protected $return_type = 'json';

	public $before_create = array( 'hash_pass' );

	protected function hash_pass($book)
	{
		if(isset($book['password'])){
		$book['password'] =md5( $book['password']);
		}
		return $book;
	}


	public $validate = array(
		array( 
			'field' => 'email', 
			'label' => 'البريد الالكتروني',
			'rules' => 'required|valid_email|is_unique[users.email]' ),
		
		array( 
			'field' => 'name', 
			'label' => 'الاسم',
			'rules' => 'required|is_special_char_clean' ),
		array( 
			'field' => 'phone', 
			'label' => 'رقم الهاتق',
			'rules' => 'required|numeric' ),
		

		array( 
			'field' => 'password', 
			'label' => 'كلمة المرور',
			'rules' => 'required' ),
		);


	public function forgot_password($email){
		if(empty($email)) return false;
      $response=$this->users_model->get_by(array('email' => $email,'enabled' => 1));      
      return $response;

		
	}

/**
	 * Update password reset sent datetime
	 *
	 * @access public
	 * @param int $account_id
	 * @return int password reset time
	 */
	function update_reset_sent_datetime($account_id)
	{
		$this->load->helper('date');

		$resetsenton = mdate('%Y-%m-%d %H:%i:%s', now());

		$this->db->update('users', array('resetsenton' => $resetsenton), array('id' => $account_id));

		return strtotime($resetsenton);
	}

	/**
	 * Remove password reset datetime
	 *
	 * @access public
	 * @param int $account_id
	 * @return void
	 */
	function remove_reset_sent_datetime($account_id)
	{
		$this->db->update('users', array('resetsenton' => NULL), array('id' => $account_id));
	}



	/**
	 * Create an account when reg using FB/Twitter
	 *
	 * @access public
	 * @param string $username
	 * @param string $medium 
	 * @return int insert id
	 */
	function create_account_for_provider($data_new )
	{

		return $this->users_model->insert($data_new);
	}

	public function update_session(){

		$id=$this->session->userdata('user_id');
		$response=$this->users_model->get($id);


		$session_data = array(
			'user_id'  =>$id,
			'name'     => $response['name'],
			'email'     => $response['email'],
			'phone'     => $response['phone'],
			);
		//only update image if NOrmal account ( not fb/twitter)
		if($response['provider']=="default"){
			$session_data['user_image']  = $response['image'];
		}


		$this->session->set_userdata($session_data);

		
	}
	
/**
	 * Update account last signed in dateime
	 *
	 * @access public
	 * @param int $account_id
	 * @return void
	 */
	function update_last_signed_in_datetime($account_id)
	{
		$this->load->helper('date');

		$this->db->update('users', array('lastsignedinon' => mdate('%Y-%m-%d %H:%i:%s', now())), array('id' => $account_id));
	}


}
