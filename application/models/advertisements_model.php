<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class advertisements_model extends MY_Model {


	protected $_table = 'advertisements';
	protected $return_type = 'array';



	public function get_fixed_ad_homepage_1()
	{
		$this->db->from('advertisements');
		$this->db->join('advertisement_locations', 'advertisement_locations.advertisement_id = advertisements.id');
		$this->db->where('advertisement_locations.location_id', 1);	
		$this->db->where('start_date <=',  date('Y-m-d'));
		$this->db->where('end_date >=', date('Y-m-d'));

		$res= $this->db->get()->result_array();
		return $res;	
		// if($res){
		// 	return $res[0];	
		// }
		// return false;
	}
	public function get_fixed_ad_homepage_2()
	{
		$this->db->from('advertisements');
		$this->db->join('advertisement_locations', 'advertisement_locations.advertisement_id = advertisements.id');
		$this->db->where('advertisement_locations.location_id', 2);	
		$this->db->where('start_date <=',  date('Y-m-d'));
		$this->db->where('end_date >=', date('Y-m-d'));

		$res= $this->db->get()->result_array();
		return $res;	
		// if($res){
		// 	return $res[0];	
		// }
		// return false;
	}
	public function get_fixed_ad_in_adview()
	{
		$this->db->from('advertisements');
		$this->db->join('advertisement_locations', 'advertisement_locations.advertisement_id = advertisements.id');
		$this->db->where('advertisement_locations.location_id', 3);	
		$this->db->where('start_date <=',  date('Y-m-d'));
		$this->db->where('end_date >=', date('Y-m-d'));

		$res= $this->db->get()->result_array();
		return $res;	
		// if($res){
		// 	return $res[0];	
		// }
		// return false;
	}
public function get_fixed_ads($location_id=false)
	{
		$this->db->from('advertisements');
		$this->db->join('advertisement_locations', 'advertisement_locations.advertisement_id = advertisements.id');
		if($location_id){
		$this->db->where('advertisement_locations.location_id', $location_id);	
		}
		$this->db->where('start_date <=',  date('Y-m-d'));
		$this->db->where('end_date >=', date('Y-m-d'));

		$res= $this->db->get()->result_array();
		return $res;	
		// if($res){
		// 	return $res[0];	
		// }
		// return false;
	}



}
