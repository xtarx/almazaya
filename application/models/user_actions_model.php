<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class user_actions_model extends MY_Model {


	protected $_table = 'user_actions';
	protected $return_type = 'array';

	public function get_likes($user_id)
	{

		$this->db->select('*,classified_ads.id as id ');
		$this->db->from('user_actions');
		$this->db->join('classified_ads', 'classified_ads.id = user_actions.classified_ad_id');
		$this->db->where('classified_ads.enabled', 1);
		$this->db->where('user_actions.user_id', $user_id);
		
		return $this->db->get()->result_array();;

	}

}
