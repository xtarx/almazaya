<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class classified_ads_model extends MY_Model {


	protected $_table = 'classified_ads';
	protected $return_type = 'array';
	public $has_many = array( 'classified_ad_images' );



	public $validate = array(
		array( 
			'field' => 'title', 
			'label' => 'العنوان',
			'rules' => 'required' ),
		array( 
			'field' => 'category_id', 
			'label' => 'category_id',
			'rules' => 'required' ),

		array( 
			'field' => 'phone', 
			'label' => 'رقم الهاتق',
			'rules' => 'required|numeric' ),

		array( 
			'field' => 'email', 
			'label' => 'البريد الالكتروني',
			'rules' => 'required|valid_email' ),
		);



  public function get_ads_by_section_id($section=false,$issue_id,$limit=false,$page=false)
  {
 //then get latest issue
      // $issue=$this->issues_model->order_by('id','desc')->limit(1)->get_all();
      // $issue=$issue[0]['id'];

            //get by section
    $this->db->select('*,classified_ads.id as id ');
    $this->db->from('classified_ads');
    $this->db->join('categories', 'categories.id = classified_ads.category_id');
    $this->db->join('sections', 'sections.id = categories.section_id');
    $this->db->where('sections.id', $section);
    $this->db->where('classified_ads.enabled', 1);
    
    //issue join
    $this->db->join('classified_ad_issues', 'classified_ad_issues.classified_ad_id = classified_ads.id');
    $this->db->where('classified_ad_issues.issue_id', $issue_id);
    //issue join

    if($limit){
      if(!$page){
        $page=1;
      }
      $this->db->limit($limit, $page-1);
    }



//pagination query
    // $this->db->limit(CLASSIFEIDS_PER_PAGE, $page-1);
  //pagination query

    $data= $this->db->get()->result_array();

    //images query
    
    for ($i = 0, $size = count($data); $i < $size; $i++) {
      $main_pic = $this->classified_ad_images_model->get_by(array(
        'classified_ad_id' => $data[$i]['id']
        ));
            //if image
      if ($main_pic) {
        $data[$i]['image'] =base_url("uploads/classified_ads/images")."/". $main_pic['image'];
        $filename=$main_pic['image'] ;
        $extension_pos = strrpos($filename, '.'); 
        $thumb = substr($filename, 0, $extension_pos) . '_thumb' . substr($filename, $extension_pos);
        $data[$i]['thumb'] =base_url("uploads/classified_ads/images/")."/".$thumb; 


      }
    }

//images query

    return $data;


  }

  public function get_ads_by_user_id($user_id,$issue_id,$limit=false,$page=false)
  {
 //then get latest issue

    if(!$issue_id){
      $issue=$this->issues_model->order_by('id','desc')->limit(1)->get_all();
      $issue_id=$issue[0]['id'];
    }

            //get by section
    $this->db->select('*,classified_ads.id as id ');
    $this->db->from('classified_ads');
    $this->db->join('categories', 'categories.id = classified_ads.category_id');
    $this->db->join('sections', 'sections.id = categories.section_id');
    

    $this->db->where('user_id', $user_id);
    $this->db->where('classified_ads.enabled', 1);

    

    if($issue_id){
    //issue join
      $this->db->join('classified_ad_issues', 'classified_ad_issues.classified_ad_id = classified_ads.id');
      $this->db->where('classified_ad_issues.issue_id', $issue_id);
    //issue join
    }
    if($limit){
      if(!$page){
        $page=1;
      }
      $this->db->limit($limit, $page-1);
    }



//pagination query
    // $this->db->limit(CLASSIFEIDS_PER_PAGE, $page-1);
  //pagination query

    $data= $this->db->get()->result_array();

    //images query
    
    for ($i = 0, $size = count($data); $i < $size; $i++) {
      $main_pic = $this->classified_ad_images_model->get_by(array(
        'classified_ad_id' => $data[$i]['id']
        ));
            //if image
      if ($main_pic) {
        $data[$i]['image'] =base_url("uploads/classified_ads/images")."/". $main_pic['image'];
        $filename=$main_pic['image'] ;
        $extension_pos = strrpos($filename, '.'); 
        $thumb = substr($filename, 0, $extension_pos) . '_thumb' . substr($filename, $extension_pos);
        $data[$i]['thumb'] =base_url("uploads/classified_ads/images/")."/".$thumb; 

      }
    }

//images query

    return $data;


  }
  public function get_ads_by_keyword($q,$issue_id,$limit=false,$page=false)
  {
 //then get latest issue

    if(!$issue_id){
      $issue=$this->issues_model->order_by('id','desc')->limit(1)->get_all();
      $issue_id=$issue[0]['id'];
    }
            //get by section
    $this->db->select('*,classified_ads.id as id ');
    $this->db->from('classified_ads');
    $this->db->join('categories', 'categories.id = classified_ads.category_id');
    $this->db->join('sections', 'sections.id = categories.section_id');
    
    $this->db->where('classified_ads.enabled', 1);


    if($issue_id){
    //issue join
      $this->db->join('classified_ad_issues', 'classified_ad_issues.classified_ad_id = classified_ads.id');
      $this->db->where('classified_ad_issues.issue_id', $issue_id);
    //issue join
    }

 //search query
    if($q){
      $this->db->like('title', $q);
      $this->db->or_like('description', $q);
    } 
    //search query


    if($limit){
      if(!$page){
        $page=1;
      }
      $this->db->limit($limit, $page-1);
    }



//pagination query
    // $this->db->limit(CLASSIFEIDS_PER_PAGE, $page-1);
  //pagination query

    $data= $this->db->get()->result_array();

    //images query
    
    for ($i = 0, $size = count($data); $i < $size; $i++) {
      $main_pic = $this->classified_ad_images_model->get_by(array(
        'classified_ad_id' => $data[$i]['id']
        ));
            //if image
      if ($main_pic) {
        $data[$i]['image'] =base_url("uploads/classified_ads/images")."/". $main_pic['image'];
        $filename=$main_pic['image'] ;
        $extension_pos = strrpos($filename, '.'); 
        $thumb = substr($filename, 0, $extension_pos) . '_thumb' . substr($filename, $extension_pos);
        $data[$i]['thumb'] =base_url("uploads/classified_ads/images/")."/".$thumb; 

      }
    }

//images query

    return $data;


  }


  public function get_ads_by_category_id($category=false,$issue_id,$limit=false,$page=false)
  {
 //then get latest issue
      // $issue=$this->issues_model->order_by('id','desc')->limit(1)->get_all();
      // $issue=$issue[0]['id'];

   $this->db->select('*,classified_ads.id as id ');
   $this->db->from('classified_ads');
   $this->db->join('categories', 'categories.id = classified_ads.category_id');

    //issue join
   $this->db->join('classified_ad_issues', 'classified_ad_issues.classified_ad_id = classified_ads.id');
   $this->db->where('classified_ad_issues.issue_id', $issue_id);
    //issue join


   $this->db->where('categories.id', $category);
   $this->db->where('classified_ads.enabled', 1);

   if($limit){
    if(!$page){
      $page=1;
    }
    $this->db->limit($limit, $page-1);
  }



//pagination query
    // $this->db->limit(CLASSIFEIDS_PER_PAGE, $page-1);
  //pagination query
  

  $data= $this->db->get()->result_array();

    //images query

  for ($i = 0, $size = count($data); $i < $size; $i++) {

    $main_pic = $this->classified_ad_images_model->get_by(array(
      'classified_ad_id' => $data[$i]['id']
      ));
            //if image
    if ($main_pic) {
      $data[$i]['image'] =base_url("uploads/classified_ads/images")."/". $main_pic['image'];
      $filename=$main_pic['image'] ;
      $extension_pos = strrpos($filename, '.'); 
      $thumb = substr($filename, 0, $extension_pos) . '_thumb' . substr($filename, $extension_pos);
      $data[$i]['thumb'] =base_url("uploads/classified_ads/images/")."/".$thumb; 

    }
  }

//images query

  return $data;



}
public function list_ads($page,$section = false, $category = false)
{


  $data['sections'] = $this->sections_model->order_by('priority','asc')->get_all();
  $data['mobawabas'] = $this->order_by('created_at','desc')->limit(10)->get_many_by(array('enabled' =>1 ,'type' =>'regular' ));

    //paid(fixed) ads

  $data['fixed_ads_homepage_1'] = $this->advertisements_model->get_fixed_ad_homepage_1();
  $data['fixed_ads_homepage_2'] = $this->advertisements_model->get_fixed_ad_homepage_2();

    //paid(fixed) ads

  
  $q=$this->input->get('q');


  $issue=$this->session->userdata('issue_number');

  if(!isset($issue) || ($issue=='')){
      //then get latest issue
    $issue=$this->issues_model->order_by('id','desc')->limit(1)->get_all();
    $issue=$issue[0]['id'];
  }

  $sort=$this->input->get('sort');


  switch ($sort) {
    case 'views':
    $this->db->order_by("classified_ads.views", "desc"); 

    break;

    default:
    $this->db->order_by("classified_ads.created_at", "desc"); 
    break;
  }

  if($issue){
    $this->db->where('classified_ads.enabled', 1);

  }



  if (!$section && !$category) {



    $data['page_title']="كل الإعلانات";


    $data['selected_section_name']="كل الأقسام";
    // $data['ads'] = $this->classified_ads_model->limit(CLASSIFEIDS_PER_PAGE, $page-1)->get_many_by( array('enabled' => 1, ));


    $this->db->select('*,classified_ads.id as id ');
    $this->db->from('classified_ads');
    //issue join
    $this->db->join('classified_ad_issues', 'classified_ad_issues.classified_ad_id = classified_ads.id');
    $this->db->where('classified_ad_issues.issue_id', $issue);
    //issue join
    $this->db->where('classified_ads.enabled', 1);
 //search query
    if($q){
      $this->db->like('title', $q);
      $this->db->or_like('description', $q);
    } 
    //search query


  //pagination query
    $this->db->limit(CLASSIFEIDS_PER_PAGE, $page-1);
  //pagination query


    $data['ads'] = $this->db->get()->result_array();




  }

  else if ($category != "") {

    $this->db->select('*,classified_ads.id as id ');
    $this->db->from('classified_ads');
    $this->db->join('categories', 'categories.id = classified_ads.category_id');
    //issue join
    $this->db->join('classified_ad_issues', 'classified_ad_issues.classified_ad_id = classified_ads.id');
    $this->db->where('classified_ad_issues.issue_id', $issue);
    //issue join


    //$this->db->where('categories.name', $category);
    // $this->db->where('categories.url', $category);
    $this->db->where('categories.id', $category);
    $this->db->where('classified_ads.enabled', 1);
 //search query
    if($q){
      $this->db->like('title', $q);
      $this->db->or_like('description', $q);
    } 
    //search query


  //pagination query
    $this->db->limit(CLASSIFEIDS_PER_PAGE, $page-1);
  //pagination query


    $data['ads'] = $this->db->get()->result_array();


    $section    = $this->sections_model->get_by(array(
      'url' => $section
      ));  

    $category_name    = $this->categories_model->get_by(array(
      'id' => $category
      ));
    $section_id = $section['id'];

    $data['categories'] = $this->categories_model->get_many_by(array(
      'section_id' => $section_id
      ));


    $data['selected_section_name']= $section['name'];
    $data['selected_section_id']= $section['id'];
    $data['selected_category_name']= $category_name['name'];
    $data['selected_category_id']= $category_name['id'];

    //set page title
    $data['page_title']=$section['name'] .' '.$category_name['name'];
    //set page title

  } else {

            //get by section
    $this->db->select('*,classified_ads.id as id ');
    $this->db->from('classified_ads');
    $this->db->join('categories', 'categories.id = classified_ads.category_id');
    $this->db->join('sections', 'sections.id = categories.section_id');
    $this->db->where('sections.url', $section);
    $this->db->where('classified_ads.enabled', 1);
    //issue join
    $this->db->join('classified_ad_issues', 'classified_ad_issues.classified_ad_id = classified_ads.id');
    $this->db->where('classified_ad_issues.issue_id', $issue);
    //issue join
     //search query
    if($q){
      $this->db->like('title', $q);
      $this->db->or_like('description', $q);
    } 
    //search query
//pagination query
    $this->db->limit(CLASSIFEIDS_PER_PAGE, $page-1);
  //pagination query



    $data['ads'] = $this->db->get()->result_array();

    $section    = $this->sections_model->get_by(array(
      'url' => $section
      ));
    $section_id = $section['id'];

    $data['categories'] = $this->categories_model->get_many_by(array(
      'section_id' => $section_id
      ));


    $data['selected_section_name']= $section['name'];
    $data['selected_section_id']= $section['id'];

    //set page title
    $data['page_title']=$section['name'];
    //set page title



  }



  for ($i = 0, $size = count($data['ads']); $i < $size; $i++) {

    $main_pic = $this->classified_ad_images_model->get_by(array(
      'classified_ad_id' => $data['ads'][$i]['id']
      ));
            //if image
    if ($main_pic) {
      $data['ads'][$i]['image'] = $main_pic['image'];
    }
  }


  return $data;

}

}
