<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class classified_ads extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('classified_ads_model');
    $this->load->model('classified_ad_images_model');
    $this->load->model('sections_model');
    $this->load->model('categories_model');
    $this->load->model('issues_model');
    $this->load->model('classified_ad_issues_model');
    $this->load->model('user_actions_model');
    $this->load->model('advertisements_model');

    // print_r($this->session->all_userdata());
  }

  public function add()
  {

    if (!is_logged()) {
      redirect('user/login');
    }

    //if not admin

    if(!is_admin($this->session->userdata('user_id'))){
      redirect('user/login');

    }

    $data['page_title']="اضافة إعلان";

    $data['sections'] = $this->sections_model->order_by('priority','asc')->get_all();
    $data['issues'] = $this->issues_model->get_all();
    $this->load->view("classified_ads/add", $data);
  }

  public function add_mobawab()
  {

    if (!is_logged()) {
      redirect('user/login/add_mobawab');
    }

    $data['page_title']="اضافة إعلان مبوب";

    $data['sections'] = $this->sections_model->order_by('priority','asc')->get_all();
    // $data['issues'] = $this->issues_model->get_all();
    $this->load->view("classified_ads/mobawab_add.php", $data);
  }


  /*
  redirects to after addiing a mobawaba add
   */
  public function done()
  {

    if (!is_logged()) {
      redirect('user/login');
    }

    $data['page_title']="اضافة إعلان نبوب";

    $data['sections'] = $this->sections_model->order_by('priority','asc')->get_all();
    // $data['issues'] = $this->issues_model->get_all();
    $this->load->view("classified_ads/notification-modal.php", $data);
  }


  
  
  public function change_issue($id)
  {

    // $this->session->set_userdata('issue_number',$id);

    // $issue_name=$this->issues_model->get($id);
    // $issue_name=$issue_name['name'];
    // $this->session->set_userdata('issue_name',$issue_name);
    // redirect_home();

    //get PDF of the selected issue by id

    $issue=$this->issues_model->get($id);


    redirect('uploads/issues/'.$issue['pdf']);
  }


  
  

  public function edit($id)
  {

    if (!is_logged()) {
      redirect('user/login');
    }


    //if ad owner
    $is_owner=$this->classified_ads_model->get_by( array(
      'id' =>$id ,
      'user_id' =>$this->session->userdata['user_id']));

    if(!$is_owner&&!is_admin($this->session->userdata('user_id'))){
      redirect_home();
    }

    $data                  = $this->classified_ads_model->with('classified_ad_images')->get($id);
    $section               = $this->categories_model->get($data['category_id']);
    $data['section_id']    = $section['section_id'];
    $section_name          = $this->sections_model->get($data['section_id']);
    $data['section_name']  = $section_name['name'];
    $data['category_name'] = $section['name'];
    $data['sections']      = $this->sections_model->order_by('priority','asc')->get_all();

    $data['issues'] = $this->issues_model->get_all();
    $data['selected_issues'] = $this->classified_ad_issues_model->get_selected_issues($id);

    $data['page_title']="تعديل إعلان";

    // print_r($data);
    $this->load->view("classified_ads/edit", $data);
  }
  public function delete()
  {

    $is=is_logged();


    if (!is_logged()) {
      header('HTTP/ 404 fail');

      return;

    }


    //if ad owner
    $is_owner=$this->classified_ads_model->get_by( array(
      'id' =>$this->input->post('id') ,
      'user_id' =>$this->session->userdata['user_id']));

    if(!$is_owner&&!is_admin($this->session->userdata('user_id'))){

      header('HTTP/ 404 fail');

      return;

    }

    $delete_response = $this->classified_ads_model->delete_by($is_owner);
    

    if($delete_response){
      header('HTTP/ 200 Success');

      return;

    }
    header('HTTP/ 404 fail');

    return;

  }
  public function do_edit($id)
  {

    $message = array(
      'category_id' => $this->input->post('category_id'),
      'title' => $this->input->post('title'),
      'description' => $this->input->post('description'),
      'phone' => $this->input->post('phone'),
      'email' => $this->input->post('email'),
      


      );


    foreach ($message as $key => $value) {

      if (!isset($value) || $value == null) {
        unset($message[$key]);
      }
    }

    //update issues
    $issues= $this->input->post('issues');
    if($issues){

  //delete old
      $this->classified_ad_issues_model->delete_by( array('classified_ad_id' => $id));
      

      foreach ($issues as $key => $issue) {
        //insert new
        $this->classified_ad_issues_model->insert(array('classified_ad_id' => $id, 'issue_id' => $issue));

      }

    }

    $response = $this->classified_ads_model->update($id, $message);
    if ($response) {

      //flush cache
    $this->cache->delete_all();

      redirect('classified_ads/view/' . $id);
    } else {
      redirect('classified_ads/edit/' .$id.'?ref=error');
    }


  }


  public function like()
  {

    if(!is_logged()){
      header('HTTP/ 404 Failed');

    }
    $data_new = $_POST;
    // print_r($data_new);
    // return;

    $data_new['user_id'] = $this->session->userdata['user_id'];
    
    // //make sure he didint insert for
    $this->load->model('user_actions_model');
    $thereBefore=$this->user_actions_model->get_by($data_new);
    

    if($thereBefore){
      //dislike ie; remove it
      $this->user_actions_model->delete_by($data_new);

      $this->db->where('id', $data_new['classified_ad_id']);
      $this->db->set('likes', 'likes-1', FALSE);
      $this->db->update('classified_ads');

      header('HTTP/ 404 Failed');


    }else{
      //insert
      $this->user_actions_model->insert($data_new);
      
      $this->db->where('id', $data_new['classified_ad_id']);
      $this->db->set('likes', 'likes+1', FALSE);
      $this->db->update('classified_ads');

      header('HTTP/ 200 Success');

      
    }
  }



  public function my_likes($user_id=1)
  {

    if (!is_logged()) {
      redirect_home();
    }

    $data['page_title']="إعلاناتي المفضلة";
    $data['sections'] = $this->sections_model->order_by('priority','asc')->get_all();

    $user_id = $this->session->userdata['user_id'];

    $data['ads']=$this->user_actions_model->get_likes($user_id);
    for ($i = 0, $size = count($data['ads']); $i < $size; $i++) {

      $main_pic = $this->classified_ad_images_model->get_by(array(
        'classified_ad_id' => $data['ads'][$i]['id']
        ));

            //print_r($main_pic);
            //if image
      if ($main_pic) {
        $data['ads'][$i]['image'] = $main_pic['image'];
      }


    }

    // print_r($data);

    $this->load->view("user/my_ads", $data);



  }
  public function index()
  {
    redirect_home();

  }

  public function add_images($id)
  {
    if (!is_logged()) {
      redirect('user/login');
    }

    $data['page_title']="إضافة صور";
    $data['sections'] = $this->sections_model->order_by('priority','asc')->get_all();

    $this->load->view("classified_ads/add_images", $data);
  }
  public function edit_images($id)
  {
    if (!is_logged()) {
      redirect('user/login');
    }

    $data['page_title']="تعديل صور";
    $data['id']       = $id;
        // print_r($data);
    $data['sections'] = $this->sections_model->order_by('priority','asc')->get_all();

    $this->load->view("classified_ads/edit_images", $data);
  }
  public function add_categories()
  {

    $sections     = $this->sections_model->order_by('priority','asc')->get_all();
    foreach ($sections as $key => $value) {

          //if doesnt have a category then add  "الكل"

      $has_cat=$this->categories_model->get_by( array('section_id' => $value['id'], ));


      if(!$has_cat){
        echo "done add";
        echo "</br>";

        $this->categories_model->insert( array(
          'section_id' => $value['id'], 
          'name' =>"الكل" , 
          'url' => "all" , 
          'enabled' => 1 , 

          ));
      }
    }



  }
  public function con2()
  {

    //flush cache
    $this->cache->delete_all();



  }
  public function con()
  {

   //phpinfo();
    //img resizeing


    $config['image_library'] = 'gd2';
    $config['source_image'] = './uploads/classified_ads/images/a0620805abf2430767955610b5d7b410.jpg';
    $image_config['maintain_ratio'] = TRUE;
    $image_config['new_image'] = './uploads/classified_ads/images/newIMG.jpg';
    $image_config['quality'] = "100%";
    $image_config['width'] = 231;
    $image_config['height'] = 154;
// $dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
// $image_config['master_dim'] = ($dim > 0)? "height" : "width";


    $this->load->library('image_lib', $config); 

    $this->image_lib->resize();



    if ( ! $this->image_lib->resize())
    {
      echo $this->image_lib->display_errors();
    }

    return;

    //img resizeing
    //

  }

  public function preload($id)
  {
    $ad         = $this->classified_ads_model->with('classified_ad_images')->get($id);
    $targetPath = getcwd() . '/uploads/classified_ads/images/';

    $result="";
        foreach ($ad['classified_ad_images'] as $file) { //get an array which has the names of all the files and loop through it 
            $obj['name'] = base_url('/uploads/classified_ads/images/'.$file['image'])  ; //get the filename in array
            $obj['size'] = filesize($targetPath . $file['image']); //get the flesize in array
            $result[]    = $obj; // copy it to another array
          }
        //header('Content-Type: application/json');
  echo  json_encode($result); // now you have a json response which you can use in client side 


}

function delete_image_post()
{

  $image_name= explode("/",$this->input->post('image_id'));


//echo "aa".end($image_name);
  $message = array(
    'user_id' => $this->session->userdata['user_id'],
    'image' => end($image_name)
    );

        //ad id
  $ad_id = $this->classified_ad_images_model->get_by(array(
    'image' => $message['image']
    ));
  if (!$ad_id) {
    header('HTTP/ 404 Failed');

  }



  $ad_id    = $ad_id['classified_ad_id'];
        //if ad owner
  $is_owner = $this->classified_ads_model->get_by(array(
    'id' => $ad_id,
    'user_id' => $message['user_id']
    ));

  if (!$is_owner&&!is_admin($this->session->userdata('user_id'))) {
    header('HTTP/ 404 Failed');
  }

  $response = $this->classified_ad_images_model->delete_by(array(
    'image' => $message['image']
    ));


  if ($response) {
    header('HTTP/ 200 Successfully ranked');
  }

  else {
    header('HTTP/ 200 Successfully ranked');
  }

}



function do_upload($classified_ad_id)
{

  if (!empty($_FILES)) {

    $tempFile  = $_FILES['file']['tmp_name'];
    $extension = '.' . end(explode(".", $_FILES["file"]["name"]));


    $fileName   = generate_token() . $extension;
    $targetPath = getcwd() . '/uploads/classified_ads/images/';
    $targetFile = $targetPath . $fileName;
    move_uploaded_file($tempFile, $targetFile);

  

    //do img thumb
    $this->createThumbnail($fileName);


    $image_data = array(
      'classified_ad_id' => $classified_ad_id,
      'image' => $fileName

      );
    $this->classified_ad_images_model->insert($image_data);
    //flush cache
    $this->cache->delete_all();

  }



}

//Create Thumbnail function
public function createThumbnail($filename)
{
  $this->load->library('image_lib');
    // Set your config up
  $config['image_library']    = "gd2";      
  $config['source_image']     = "uploads/classified_ads/images/" .$filename;      
    // $config['source_image']     = $filename;      
    // $config['source_image']     = "uploads/classified_ads/t/1.jpg";      
  $config['create_thumb']     = TRUE;      
  $config['maintain_ratio']   = TRUE;      
  $config['width'] = "200";      
  $config['height'] = "200";

  $this->image_lib->initialize($config);
    // Do your manipulation

  if(!$this->image_lib->resize())
  {
    echo $this->image_lib->display_errors();
  } 
  $this->image_lib->clear();     
}


public function get_categories($section_id, $selected = false)
{

  $categories = $this->categories_model->get_many_by(array(
    'section_id' => $section_id
    ));

        //print_r($categories);

  echo $this->selectify($categories, $selected);
}


public function fix_sections_priority()
{

  $sections = $this->sections_model->order_by('priority','asc')->get_all();

  $max=24;
  foreach ($sections as $key => $value) {


    $this->db->set('priority',$max);
    $this->db->where('id', $value['id']);
    $this->db->update('sections');

    $max--;

  }



}


public function selectify($array, $selected = false)
{
  $result = "";
  foreach ($array as $key => $value) {

    $id   = $value['id'];
    $name = $value['name'];
    $result .= '<option value="' . $id.'"';
    if ($id == $selected)
      $result .= 'selected="selected" ';

    $result .= '>' . $name . '</option>';


  }
  return $result;

}



public function view($id)
{

  $data = $this->classified_ads_model->with('classified_ad_images')->get_by( array('id' => $id,'enabled' => 1 ));
  
  if(!$data){
    redirect_home();
  }

  $data['page_title']=substr($data['title'], 0,100);
    //paid(fixed) ads

  $data['fixed_ad_adview'] = $this->advertisements_model->get_fixed_ad_in_adview();

    //paid(fixed) ads

  $section               = $this->categories_model->get($data['category_id']);
  $data['section_id']    = $section['section_id'];
  $section_name          = $this->sections_model->get($data['section_id']);
  $data['section_name']  = $section_name['name'];
  $data['section_url']  = $section_name['url'];
  $data['category_name'] = $section['name'];
  $data['category_url'] = $section['url'];
  $data['sections']      = $this->sections_model->order_by('priority','asc')->get_all();

  $data['is_owner'] = false;
  $data['liked']=false;
  if (is_logged()) {

    $data['liked']=$this->user_actions_model->get_by(array('user_id'=>$this->session->userdata['user_id'],'classified_ad_id'=>$id));


    $is_owner = $this->classified_ads_model->get_by(array(
      'id' => $id,
      'user_id' => $this->session->userdata['user_id']
      ));

    if ($is_owner ||is_admin($this->session->userdata['user_id'])) {
      $data['is_owner'] = true;
    }
  } 
  // print_r($data);

    // //upadte # views
    // $this->db->set($tab . '_views' , $tab . '_views+1', FALSE);
    // $this->db->where('id', $id);
    // $this->db->update($no_date_table_name);

  $this->db->set('views', 'views+1', FALSE);
  $this->db->where('id', $id);
  $this->db->update('classified_ads');



  $this->load->view("classified_ads/view", $data);
}


public function list_ads($section =false, $category = false)
{

  if(!$section){
    redirect('classified_ads/list_ads/vip');
  }

  $page=$this->input->post('p');
  if(!$page){
    $page=1;
  }

  // $data=$this->classified_ads_model->list_ads($page,$section,$category);
  $data=$this->cache->model('classified_ads_model', 'list_ads',array($page, $section, $category) );


  if($page>1){
    echo json_encode($data);
    return;
  }

  // print_r($data);

  $this->load->view("classified_ads/list", $data);

}
public function search($section = false, $category = false)
{
    //search by section ,  category , if both false then all





}

public function do_add()
{


  $data_new = $_POST;


  // print_r($data_new);
  // return;
//  exit();
  $response = $this->classified_ads_model->insert(array(
    'user_id' => $this->session->userdata['user_id'],

    'title' => $data_new['title'],
    'description' => $data_new['description'],
    'category_id' => $data_new['category_id'],
    'phone' => $data_new['phone'],
    'email' => $data_new['email']
    ));
  if ($response) {

      //add issues      
    $issues=$data_new['issues'];
    foreach ($issues as $key => $issue) {
      $this->classified_ad_issues_model->insert(array('classified_ad_id' => $response, 'issue_id' => $issue));
    }

    //if type was 19 (mobawab ) then skip images

    //flush cache
    $this->cache->delete_all();

    if($data_new['type']==19){
      redirect('classified_ads/view/' . $response);

    }else{

      redirect('classified_ads/add_images/' . $response);
    }
  } else {
    echo validation_errors(); 
    // redirect('classified_ads/add?ref=error');
  }
}


public function do_add_mobawab()
{


  $data_new = $_POST;


  // print_r($data_new);
  // return;
//  exit();
  $response = $this->classified_ads_model->insert(array(
    'user_id' => $this->session->userdata['user_id'],

    'title' => $data_new['title'],
    'description' => $data_new['description'],
    'category_id' => $data_new['category_id'],
    'phone' => $data_new['phone'],
    'email' => $data_new['email'],
    'type' => 'regular',
    'enabled' => 0,
    ));
  if ($response) {

      //add issues      
      //then get latest issue
    $issue=$this->issues_model->order_by('id','desc')->limit(1)->get_all();
    $issue=$issue[0]['id'];

    $this->classified_ad_issues_model->insert(array('classified_ad_id' => $response, 'issue_id' => $issue));
    
    //flush cache
    $this->cache->delete_all();

    redirect('classified_ads/done');
    // redirect('classified_ads/add_images/' . $response);
  } else {
    echo validation_errors(); 
    // redirect('classified_ads/add?ref=error');
  }
}





}