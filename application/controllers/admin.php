<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');



		if (!is_logged()) {
			redirect('user/login');
		}

    //if not admin

		if(!is_admin($this->session->userdata('user_id'))){
			redirect('user/login');

		}

		
	}

	public function _example_output($output = null)
	{
	//	$this->load->view('example.php',$output);
		//$output->title="لوحة التحكم";

		$this->load->view('admin/index',$output);

	}

	
	
	public function index()
	{
		redirect('admin/users');

	}

	public function users()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('users');
			$crud->unset_columns('password','image','provider','provider','lastsignedinon','resetsenton');
			$crud->fields('email', 'name', 'phone' ,  'enabled',  'role' );
			$crud->unset_add();

			$crud->display_as('email','البريد الالكتروني');
			$crud->display_as('name','الاسم');
			$crud->display_as('phone','رقم التليفون');
			$crud->display_as('enabled','الحالة');
			$crud->display_as('role','مدير');
			$crud->display_as('created_at','تاريخ التسجيل');
			



			$output = $crud->render();
			$output->bread="المستخدمين";
			$output->bread_link="users";

			
			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function classified_ads()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->where('type',"vip");
			$crud->set_table('classified_ads');
			$crud->unset_columns('description','image','views','created_at','likes','dislikes','type','category_id');
			$crud->unset_fields('description','image','views','likes','dislikes','created_at','type','category_id');
			//$crud->unset_fields('views');

			// $crud->set_relation('category_id','categories','name');
			// $crud->display_as('category_id','فئة');
			
			$crud->set_relation('user_id','users','name');
			$crud->set_relation_n_n('issues', 'classified_ad_issues','issues', 'classified_ad_id', 'issue_id', 'name');

			$crud->unset_add();

			$crud->display_as('user_id','المستخدم');
			$crud->display_as('title','العنوان');
			$crud->display_as('description','الوصف');
			$crud->display_as('phone','رقم التليفون');
			$crud->display_as('email','البريد الالكتروني');
			$crud->display_as('available','متوفر');
			$crud->display_as('likes','الاعجاب');
			$crud->display_as('dislikes','عدم الاعجاب');
			$crud->display_as('enabled','الحالة');
			$crud->display_as('created_at','تاريخ التسجيل');
			$crud->display_as('start_date','تاريخ البدء');
			$crud->display_as('end_date','تاريخ الانتهاء');
			$crud->display_as('issues','الاعداد');
			


			$output = $crud->render();
			$output->bread="الاعلانات المبوبة";
			$output->bread_link="classified_ads";


			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function mobawaba_ads()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->where('type',"regular");

			$crud->set_table('classified_ads');
			$crud->unset_columns('description','image','views','created_at','likes','dislikes','type','available','category_id');
			$crud->unset_fields('description','image','views','likes','dislikes','created_at','type','available','category_id');
			//$crud->unset_fields('views');

			// $crud->set_relation('category_id','categories','name');
			// $crud->display_as('category_id','فئة');
			
			$crud->set_relation('user_id','users','name');
			$crud->set_relation_n_n('issues', 'classified_ad_issues','issues', 'classified_ad_id', 'issue_id', 'name');

			$crud->unset_add();

			$crud->display_as('user_id','المستخدم');
			$crud->display_as('title','العنوان');
			$crud->display_as('description','الوصف');
			$crud->display_as('phone','رقم التليفون');
			$crud->display_as('email','البريد الالكتروني');
			$crud->display_as('available','متوفر');
			$crud->display_as('likes','الاعجاب');
			$crud->display_as('dislikes','عدم الاعجاب');
			$crud->display_as('enabled','الحالة');
			$crud->display_as('created_at','تاريخ التسجيل');
			$crud->display_as('start_date','تاريخ البدء');
			$crud->display_as('end_date','تاريخ الانتهاء');
			$crud->display_as('issues','الاعداد');
			


			$output = $crud->render();
			$output->bread="الاعلانات المبوبة";
			$output->bread_link="mobawaba_ads";


			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function issues()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('issues');

			$crud->unset_columns('created_at');
			$crud->unset_fields('created_at');

			$crud->display_as('name','الاسم');
			$crud->display_as('number','الرقم');
			$crud->display_as('pro_date','تاريخ الانتاج');
			$crud->display_as('pub_date','تاريخ النشر');
			$crud->display_as('pdf','pdf');
			$crud->display_as('invoice','فواتير الطباعة');

			$crud->set_field_upload('pdf','uploads/issues');
			$crud->set_field_upload('invoice','uploads/invoices/');


			//validations & required fields

			$crud->required_fields('name','number','pro_date','pub_date');
			$crud->set_rules('name','الاسم','required|is_special_char_clean');
			$crud->set_rules('number','الرقم','required|is_natural_no_zero');
			$crud->set_rules('pro_date','تاريخ الانتاج','required|validate_date_reg');
			$crud->set_rules('pub_date','تاريخ النشر','required|validate_date_reg');

			//validations & required fields

			$output = $crud->render();

			$output->bread="الاعداد";
			$output->bread_link="issues";


			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function sections()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('sections');

//			$crud->unset_columns('password');

//			$crud->fields('email', 'name', 'phone' ,  'enabled' );

			$crud->display_as('name','الاسم');
			$crud->display_as('icon','الصورة');
			$crud->display_as('url','الرابط بالحروف الانجليزية');
			$crud->display_as('enabled','الحالة');
			$crud->display_as('priority','الترتيب');

			$crud->set_field_upload('icon','uploads/sections');

			//sort
			$crud->columns('priority','name', 'url', 'enabled');
			$crud->fields('name', 'url', 'enabled');
			

			$crud->callback_column('move_up_down', array($this, 'populate_up_down'));
			$crud->order_by('priority');
			$this->session->set_userdata('callableAction', site_url(). 'admin/updatePosition/sections');
			$this->session->set_userdata('primary_key', 'id');
			$crud->set_js("index.php/admin/dragdrop_js");			

			//sort

			//validations & required fields

			// $crud->required_fields('name','icon','url');
			$crud->required_fields('name','url');
			$crud->set_rules('name','الاسم','required|is_special_char_clean');
			$crud->set_rules('url','الرابط بالحروف الانجليزية','required|alpha_numeric');

			//validations & required fields


			$output = $crud->render();
			$output->bread="الأقسام";
			$output->bread_link="sections";
			//$output->msg="اسحبها (drag and drop) لتغيير الترتيب";




			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function categories()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('categories');
			$crud->set_relation('section_id','sections','name');
			$crud->display_as('section_id','القسم');


			$crud->display_as('name','الاسم');
			$crud->display_as('color','اللون');
			$crud->display_as('icon','الصورة');
			$crud->display_as('url','الرابط بالحروف الانجليزية');	
			$crud->display_as('enabled','الحالة');
			$crud->set_field_upload('icon','uploads/categories');



			//validations & required fields

			$crud->required_fields('name','url');
			$crud->set_rules('name','الاسم','required|is_special_char_clean');
			// $crud->set_rules('color','اللون','required|is_special_char_clean');
			$crud->set_rules('url','الرابط بالحروف الانجليزية','required|alpha_numeric');

			//validations & required fields


			$output = $crud->render();

			$output->bread="الفئات";
			$output->bread_link="categories";


			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function advertisements()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('advertisements');
			
			$crud->unset_columns('description','image','thumbnail','link','created_at','likes','dislikes','views');
			$crud->unset_fields('thumbnail','created_at','likes','dislikes','views','title');

			//$crud->fields('user_id');


			$crud->set_relation_n_n('ad_places', 'advertisement_locations','locations', 'advertisement_id', 'location_id', 'name');
			$crud->set_relation('user_id','users','name');

			$crud->display_as('user_id','المستخدم');
			$crud->display_as('title','العنوان');
			$crud->display_as('description','الوصف');
			$crud->display_as('image','الصورة');
			$crud->display_as('phone','رقم التليفون');
			$crud->display_as('email','البريد الالكتروني');
			$crud->display_as('link','الرابط');
			$crud->display_as('likes','الاعجاب');
			$crud->display_as('dislikes','عدم الاعجاب');
			$crud->display_as('views','المشاهدات');
			$crud->display_as('enabled','الحالة');
			$crud->display_as('created_at','تاريخ التسجيل');
			$crud->display_as('start_date','تاريخ البدء');
			$crud->display_as('end_date','تاريخ الانتهاء');
			$crud->display_as('created_at','تاريخ البدء');
			$crud->display_as('ad_places','الاماكن');

			$crud->set_field_upload('image','uploads/static_ads');



			//validations & required fields

			$crud->required_fields('user_id','start_date','end_date','image');
			// $crud->set_rules('name','الاسم','required|is_special_char_clean');
			$crud->set_rules('url','الرابط بالحروف الانجليزية','required|alpha_numeric');
			$crud->set_rules('start_date','تاريخ البدء','required|validate_date_reg');
			$crud->set_rules('end_date','تاريخ الانتهاء','required|validate_date_reg');

			//validations & required fields




			$output = $crud->render();

			$output->bread="الاعلانات الثابتة";
			$output->bread_link="advertisements";


			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function advertisement_locations()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('advertisement_locations');
			$crud->set_relation('advertisement_id','advertisements','title');
			$crud->display_as('advertisement_id','advertisement');



			$output = $crud->render();
			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}	

	public function locations()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('locations');
			$crud->fields('name');
			$crud->unset_columns('id','created_at');

			$crud->display_as('name','الاسم');


			//validations & required fields

			$crud->required_fields('name');
			$crud->set_rules('name','الاسم','required|is_special_char_clean');

			//validations & required fields




			$output = $crud->render();

			$output->bread="الاماكن";
			$output->bread_link="locations";


			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}	

	public function super_user()
	{
		
		$session_data = array(
			'is_admin'  =>true,
			);

		$this->session->set_userdata($session_data);
		redirect('classified_ads/list_ads/Fashion');


	}

//sort

/*  This function is to be called to update the table where it is independant ... without a group / foriegn key of data. 
*   This will update the data totally on the table (as implemented in the shared example)
*/
function updatePosition($table, $sourceId, $distance, $direction) {

	$this->load->library('Priority_manager');
	$manager = new Priority_manager();
	$manager->setTable($table);
	$manager->setPriorityField('priority');

	switch ($direction) {
		case 'up' :
		$manager->moveUpBy($sourceId, $distance);
		break;
		case 'down' :
		$manager->moveDownBy($sourceId, $distance);
		break;
		case 'top' :
		$manager->moveToTop($sourceId);
		break;
		case 'bottom' :
		$manager->moveToBottom($sourceId);
		break;
		case 'default' :
		$manager->moveTo($sourceId, $distance);
		break;
	}
}

/*  This function is to be called to update the table where it is having a group / foriegn key of data. 
*   This will be a good example for tables like products along with categories. So category_id becomes foreign key.
*   All / any new record that gets created will be prioritized along with the category id
*/  
function updateGroupPosition($table, $group, $sourceId, $distance, $direction) {

	$this->load->library('Priority_manager');
	$manager = new Priority_manager();
	$manager->setTable($table);
	$manager->setGroupField($group);
	$manager->setPriorityField('priority');

	switch ($direction) {
		case 'up' :
		$manager->moveUpBy($sourceId, $distance);
		break;
		case 'down' :
		$manager->moveDownBy($sourceId, $distance);
		break;
		case 'top' :
		$manager->moveToTop($sourceId);
		break;
		case 'bottom' :
		$manager->moveToBottom($sourceId);
		break;
		case 'default' :
		$manager->moveTo($sourceId, $distance);
		break;
	}
}

/*  This function generates dynamic js for drag drop ajax calls */
function dragdrop_js() {
	$js = '
	var startPosition;
	var endPosition;
	var itemBeingDragged;
	var allIds = new Array();


	function makeAjaxCall(_url) {
		/* Send the data using post and put the results in a div */
		$.ajax({
			url: _url,
			type: "get",
			success: function(){
				$(".pReload").click();
				makeTableSortable();
			},
			error:function(){
				alert("There was a failure while repositioning the element");
			}   
		});
}

function moveUp(sourceId) {
	url="' . $this->session->userdata('callableAction') . '/" + sourceId +"/1/up";
	makeAjaxCall(url);
}

function moveDown(sourceId) {
	url="' . $this->session->userdata('callableAction') . '/" + sourceId +"/1/down";
	makeAjaxCall(url);
}

function moveToTop(sourceId) {
	url="' . $this->session->userdata('callableAction') . '/" + sourceId +"/1/top";
	makeAjaxCall(url);
}

function moveToBottom(sourceId) {
	url="' . $this->session->userdata('callableAction') . '/" + sourceId +"/1/bottom";
	makeAjaxCall(url);
}

        // Return a helper with preserved width of cells
var fixHelper = function(e, ui) {
	ui.children().each(function() {
		$(this).width($(this).width());
	});
return ui;
};

function makeTableSortable() {

	$(".groceryCrudTable  tbody").sortable(
	{


		helper: fixHelper,
		cursor : "move",
		create: function(event, ui) {
			allRows = $( ".groceryCrudTable  tbody" ).sortable({ items: "> tr" }).children();
			// console.log(allRows);
			for(var i=0; i< allRows.length; i++) {
				var _row = allRows[i];
				_id = _row.attributes["data_id"].value;
                        //_id = _id.substr(4);
				allIds.push(_id);
                      //  console.log("Pushed - " + _id);
			}
		},
		start : function(event, ui) {
			startPosition = ui.item.prevAll().length + 1;
			itemBeingDragged = ui.item.attr("data_id");
		},
		update : function(event, ui) {
			endPosition = ui.item.prevAll().length + 1;
			if(startPosition != endPosition) {
				if(startPosition > endPosition) {
					distance = startPosition - endPosition;
					url="' . $this->session->userdata('callableAction') . '/" + itemBeingDragged +"/" + distance + "/up";
					makeAjaxCall(url);
				} else {
					distance = endPosition - startPosition;
					url="' . $this->session->userdata('callableAction') . '/" + itemBeingDragged +"/" + distance + "/down";
					makeAjaxCall(url);
				}
			}
		}
	})
}

window.onload = function() {
	makeTableSortable();
};

';
header("Content-type: text/javascript");
echo $js;
}

/*  This function is used to do a reset of positions on a table. If it hasa group fields (ex. category_id) 
*   pass it along with the call. It will make sure the categorization is done based on the group_field provided.
*   If you want to reposition values of rows only for a specific group, pass up the group_value along with the call
*   ex. resetPositions('products', 'category_id', '1'); - This will reposition all the rows with category_id=1
*/
function resetPositions($table, $group_field=FALSE, $group_value=FALSE) {
	$this->load->library('Priority_manager');
	$manager = new Priority_manager();
	$manager->setTable($table);
	$manager->setGroupField($group_field);
	$manager->setPriorityField('priority');
	$manager->rearrangePriorities($group_value);
}

/*  This is a column callback function to show up the arrows. */
public function populate_up_down($value, $row) {
	$primary_key = $this->session->userdata('primary_key');
	$str = "<a href='javascript:moveToTop(" . $row->$primary_key . ")'><img src='" . base_url() . "assets/images/navigate-top-icon.png'></a>";
	$str .= "<a href='javascript:moveUp(" . $row->$primary_key . ")'><img src='" . base_url() . "assets/images/navigate-up-icon.png'></a>";
	$str .= "<a href='javascript:moveDown(" . $row->$primary_key . ")'><img src='" . base_url() . "assets/images/navigate-down-icon.png'></a>";
	$str .= "<a href='javascript:moveToBottom(" . $row->$primary_key . ")'><img src='" . base_url() . "assets/images/navigate-bottom-icon.png'></a>";
	return $str;
}


//sort


}