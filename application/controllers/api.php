<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Api extends REST_Controller
{


    public function __construct() {
        parent::__construct();
        //header('Access-Control-Allow-Origin: *');
        $this->load->model('users_model');
        $this->load->model('sections_model');
        $this->load->model('categories_model');
        $this->load->model('classified_ads_model');
        $this->load->model('issues_model');
        $this->load->model('advertisements_model');
        $this->load->model('classified_ad_issues_model');
        $this->load->model('classified_ad_images_model');

    }

    //user module


    function user_register_post()
    {

        $message = array(
         'email' => $this->post('email'),
         'password' => ($this->post('password')),
         'name' => $this->post('name'),
         'phone' => $this->post('phone'),

         );
        $response =  $this->users_model->insert($message);


        if($response)
        {
            if(isset($_FILES['image'])){
            //upload picture
                $tempFile = $_FILES['image']['tmp_name'];
                $extension = '.' . end(explode(".", $_FILES["image"]["name"]));
                $filename = generate_token() . $extension;
                $result = move_uploaded_file($tempFile, "uploads/users/images/" . $filename);
            }
            $this->response( array('user_id' =>$response ), 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(null, 404);
        }

    }

    function user_image_post()
    {

        $user_id = $this->post('user_id');
        $tempFile = $_FILES['image']['tmp_name'];
        $extension = '.' . end(explode(".", $_FILES["image"]["name"]));
        $filename = generate_token() . $extension;
        $result = move_uploaded_file($tempFile, "uploads/users/images/" . $filename);


        if ($result) {

            $this->users_model->update($user_id, array('image' => $filename ));

            $this->response(null, 200); // 200 being the HTTP response code
        } else {
            $this->response(null, 404);
        }



    }

    function user_login_post()
    {


        $response = $this->users_model->get_by(array('email' => $this->post('email'),
         'password' => md5($this->post('password')))); 

        if($response)
        {
            if($response['image']){
                $response['image'] =base_url("uploads/users/images")."/".$response['image'];      

            }
            $this->response($response, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(null, 404);
        }

    }
    
    function user_edit_post()
    {

        $message = array(
         'email' => $this->post('email'),
         'password' => $this->post('password'),
         'name' => $this->post('name'),
         'phone' => $this->post('phone'),

         );

        foreach ($message as $key => $value) {

            if(!isset($value)||$value == null)
            {
                unset($message[$key]);
            }
        }
        //if password set md5
        //
        if(isset($message['password'])){
            $message['password']=md5($message['password']);
        }

        $response =  $this->users_model->update($this->post('user_id'),$message);

        if($response)
        {
            $this->response(null, 200); // 200 being the HTTP response code
        }

        else
        {

            $this->response(null, 404);
        }

    }
    

    function user_forgot_password_post()
    {

        $response =  $this->users_model->forgot_password($this->post('email'));

        // print($response);
        if($response)
        {
            $this->response(null, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(null, 404);
        }

    }
    

    //end user module


//sections


    function sections_get()
    {


        $response =  $this->sections_model->with('categories')->get_many_by( array('enabled' => 1 ));

        if($response)
        {


    //images query
            for ($i = 0, $size = count($response); $i < $size; $i++) {
                if($response[$i]['icon'] ){
                    $response[$i]['icon'] =base_url("uploads/sections/")."/".$response[$i]['icon']; 
                }   


                foreach ($response[$i]['categories'] as $key => $category) {
                 if( $category['icon']){
                     $response[$i]['categories'][$key]['icon'] =base_url("uploads/categories")."/".$category['icon'];      
                 }
             }


         }   




    //images query

            $this->response($response, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(null, 404);
        }

    }

    function section_get($id)
    {


        $response =  $this->sections_model->with('categories')->get($id);

        if($response)
        {

            // print_r($response);
            if($response['icon']){
                $response['icon'] =base_url("uploads/sections/")."/".$response['icon'];      
            }

            $this->response($response, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(null, 404);
        }

    }

    function category_get($id)
    {

        $response =  $this->categories_model->get($id);

        if($response)
        {
            if( $response['icon']){
                $response['icon'] =base_url("uploads/categories")."/".$response['icon'];      
            }
            $this->response($response, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(null, 404);
        }

    }




    function classified_ad_add_post()
    {

        $message = array(
         'user_id' => $this->post('user_id'),
         'category_id' => $this->post('category_id'),
         'title' => $this->post('title'),
         'description' => $this->post('description'),
         'phone' => $this->post('phone'),
         'email' => $this->post('email'),
         'type' => 'regular',
         'enabled' => 0,

         );
        $response =  $this->classified_ads_model->insert($message);


        if($response)
        {
              //add issues      
      //then get latest issue
            $issue=$this->issues_model->order_by('id','desc')->limit(1)->get_all();
            $issue=$issue[0]['id'];

            $this->classified_ad_issues_model->insert(array('classified_ad_id' => $response, 'issue_id' => $issue));


            //flush cache
            $this->cache->delete_all();


            //returns id 
            $this->response(array('id' => $response), 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(null, 404);
        }

    }

    function classified_ad_get($id)
    {

        $response =  $this->classified_ads_model->with('classified_ad_images')->get($id);



        if($response)
        {

             //images query
            for ($i = 0, $size = count($response['classified_ad_images']); $i < $size; $i++) {
                if($response['classified_ad_images'][$i]['image'] ){
                    $response['classified_ad_images'][$i]['image'] =base_url("uploads/classified_ads/images/")."/".$response['classified_ad_images'][$i]['image']; 
                    $filename=$response['classified_ad_images'][$i]['image'] ;
              $extension_pos = strrpos($filename, '.'); // find position of the last dot, so where the extension starts
              $thumb = substr($filename, 0, $extension_pos) . '_thumb' . substr($filename, $extension_pos);
              $response['classified_ad_images'][$i]['thumb'] =$thumb; 


          }     
      }
    //images query


            $this->response($response, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(null, 404);
        }

    } 


    function paid_advertisements_get($id=false)
    {


        $response =  $this->advertisements_model->get_fixed_ads($id);

        if($response)
        {
            for ($i = 0, $size = count($response); $i < $size; $i++) {
             if($response[$i]['image']){
                $response[$i]['image'] =base_url("uploads/static_ads/")."/".$response[$i]['image'];      
            }
        }
            $this->response($response, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(null, 404);
        }

    } 


    function classified_ads_in_category_get($category_id,$issue_id,$limit=false,$page=false)
    {


        // $response =  $this->classified_ads_model->get_ads_by_category_id($category_id,$issue_id,$limit,$page);
        $response=$this->cache->model('classified_ads_model', 'get_ads_by_category_id',array($category_id,$issue_id,$limit,$page) );


        if($response)
        {
            $this->response($response, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(null, 404);
        }

    }

    function classified_ads_in_section_get($section_id,$issue_id,$limit=false,$page=false)
    {

        // $response =  $this->classified_ads_model->get_ads_by_section_id($section_id,$issue_id,$limit,$page);
        $response=$this->cache->model('classified_ads_model', 'get_ads_by_section_id',array($section_id,$issue_id,$limit,$page) );



        if($response)
        {
            $this->response($response, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(null, 404);
        }

    }
    
    function classified_ads_by_user_get($user_id,$issue_id=false,$limit=false,$page=false)
    {

        $response =  $this->classified_ads_model->get_ads_by_user_id($user_id,$issue_id,$limit,$page);

        if($response)
        {
            $this->response($response, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(null, 404);
        }

    }

    function classified_ads_by_keyword_post()
    {


        $keyword=$this->post('q');
        $issue_id=$this->post('issue_id');
        $limit=$this->post('limit');
        $page=$this->post('page');

        $response =  $this->classified_ads_model->get_ads_by_keyword($keyword,$issue_id,$limit,$page);

        if($response)
        {
            $this->response($response, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(null, 404);
        }

    }


    function issues_get()
    {

        $response =   $this->issues_model->order_by('id','desc')->limit(1)->get_all();



        if($response)
        {

            if($response[0]['pdf']){
                $response[0]['pdf'] =base_url("uploads/issues/")."/".$response[0]['pdf'];      
            }
            if($response[0]['invoice']){
                $response[0]['invoice'] =base_url("uploads/invoices/")."/".$response[0]['invoice'];      
            }


            $this->response($response, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(null, 404);
        }

    }



}