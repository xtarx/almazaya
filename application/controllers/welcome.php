<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

public function __construct(){
		parent::__construct();

        // To use site_url and redirect on this controller.
        $this->load->helper('url');
	}



	public function login(){

		$this->load->library('facebook'); // Automatically picks appId and secret from config
        // OR
        // You can pass different one like this
        //$this->load->library('facebook', array(
        //    'appId' => 'APP_ID',
        //    'secret' => 'SECRET',
        //    ));

		$user = $this->facebook->getUser();
        
        if ($user) {
            try {
                $data['user_profile'] = $this->facebook->api('/me');
            } catch (FacebookApiException $e) {
                $user = null;
            }
        }else {
            $this->facebook->destroySession();
        }

        if ($user) {

            $data['logout_url'] = site_url('welcome/logout'); // Logs off application
            // OR 
            // Logs off FB!
            // $data['logout_url'] = $this->facebook->getLogoutUrl();

        } else {
            $data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('welcome/login'), 
                'scope' => array("email") // permissions here
            ));
        }
        // print_r($data);
        $this->session->set_userdata('lebon', 'useracc');
        redirect("user");
        $this->load->view('fb_login',$data);

	}

    public function logout(){

        $this->load->library('facebook');

        // Logs off session from website
        $this->facebook->destroySession();
        // Make sure you destory website session as well.

        redirect('welcome/login');
    }




	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function font()
	{

		$im = imagecreatefromjpeg('000.jpg');

// get the image size
		$w = imagesx($im);
		$h = imagesy($im);
$grey = imagecolorallocate($im, 128, 128, 128);

// place some text (top, left)

$text = 'Testing...';
// Replace path by your own font path
$font = 'arial.ttf';

// Add some shadow to the text
imagettftext($im, 20, 0, 11, 21, $grey, $font, $text);



		imageJpeg($im, "001.jpg", 85);
		imagedestroy($im);


	}
	public function index()
	{
		$message = array(
			'email' =>"hh",
			'password' => '',
			'name' => "mmmm",
			'phone' => "",

			);


		print_r($message);

		foreach ($message as $key => $value) {

			if(!isset($value)||$value == null)
			{
				unset($message[$key]);
			}
		}
		print_r($message);

        // //if password set md5
        // //
        // if(isset($message['password'])){
        //     $message['password']=md5($message['password']);
        // }


	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */