<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class user extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('users_model');
		// $this->output->enable_profiler(TRUE);
		// $this->session->set_userdata('lebon', 'useracc');
		// print_r($this->session->all_userdata());
		// exit();

	}

	public function register() {

		if (is_logged()) {
			redirect_home();
		}

		$data['page_title'] = "تسجيل الحساب";

		$this->load->view("user/register", $data);
	}

	public function sign_user_in($response) {

		//handling image and defaulta vatar
		$image = "default_avatar.png";
		if ($response['image']) {
			$image = $response['image'];
		}
		$image_url = base_url() . 'uploads/users/images/' . $image;
		$session_data = array(
			'user_id' => $response['id'],
			'name' => $response['name'],
			'email' => $response['email'],
			'phone' => $response['phone'],
			'user_image' => $image_url,
			);
		$this->users_model->update_last_signed_in_datetime($response['id']);

		$this->session->set_userdata($session_data);

	}

	public function forgot_password() {

		if (is_logged()) {
			redirect_home();
		}

		$data['page_title'] = "تسجيل الحساب";

		$this->load->view("user/forgot_password", $data);
	}

	public function reset_password() {
// echo "string"
		// Get account by email
		$account = $this->users_model->get($this->input->get('id'));
		if ($account) {

			// Check if reset password has expired
			if (now() < (strtotime($account['resetsenton']) + $this->config->item("password_reset_expiration"))) {

				// Check if token is valid
				if ($this->input->get('token') == sha1($account['id'] . strtotime($account['resetsenton']) . $this->config->item('password_reset_secret'))) {
					// Remove reset sent on datetime
					$this->users_model->remove_reset_sent_datetime($account['id']);

					// Upon sign in, redirect to change password page
					// $this->session->set_userdata('sign_in_redirect', 'account/account_password');

					// Run sign in routine

					$this->sign_user_in($account);
					redirect('user/change_password');

				}
			}

			redirect_home();

		}
		redirect_home();
		// Load reset password unsuccessful view
		// $this->load->view('account/reset_password_unsuccessful', isset($data) ? $data : NULL);

	}

	public function do_forgot() {

		$response = $this->users_model->get_by(array('email' => $this->input->post('email'), 'enabled' => 1 ,'provider' => 'default'));
		if ($response) {

			$user_id = $response['id'];
			// Set reset datetime
			$time = $this->users_model->update_reset_sent_datetime($user_id);

		
			// Generate reset password url
			$password_reset_url = site_url('user/reset_password?id=' . $user_id . '&token=' . sha1($user_id . $time . $this->config->item('password_reset_secret')));
			
			$html="<a href='".$password_reset_url ."'>".$password_reset_url ."<a>";
			// echo $html;
			// return;
			sendmail($response['email'],"استرجاع كلمة السر",$html);
			$this->load->view("user/password_sent");

			// echo $password_reset_url;

		} else {
			$data['redirection'] = $this->input->post('redirection', TRUE);
			$data['error'] = "برجاء التأكد من البريد الالكتروني";
			$this->load->view("user/forgot_password", $data);

		}

	}

	

	public function edit() {
		if (!is_logged()) {
			redirect_home();
		}
		$data = $this->users_model->get_by(array('id' => $this->session->userdata('user_id')));
		$data['page_title'] = "تعديل الحساب";
		// print_r($data);
		$this->load->view("user/edit", $data);
	}

	public function change_password() {
		if (!is_logged()) {
			redirect_home();
		}
		$data['page_title'] = "تعديل الحساب";
		// print_r($data);
		$this->load->view("user/change_password", $data);
	}

	public function do_change_password() {
		if (!is_logged()) {
			redirect_home();
		}

		$data['page_title'] = "تعديل الحساب";

		$response = $this->users_model->update($this->session->userdata('user_id'), array('password' => md5($this->input->post('password'))));

		redirect_home();

	}

	public function do_edit() {
		// print_r($_POST);
		// exit();

// return;
		$did_upload = false;
		$upload_success = false;

		// return;
		if (!is_logged()) {
			redirect_home();
		}

		$message = array(
			'email' => $this->input->post('email'),
			'password' => $this->input->post('password'),
			'name' => $this->input->post('name'),
			'phone' => $this->input->post('phone'),

			);

		foreach ($message as $key => $value) {

			if (!isset($value) || $value == null) {
				unset($message[$key]);
				unset($_POST[$key]);

			}
		}
		//if password set md5
		//
		if (isset($message['password'])) {
			$message['password'] = md5($message['password']);
		}
		// print_r($message);

		//if values are the sampe unset
		$old_data = $this->users_model->get_by(array('id' => $this->session->userdata('user_id')));

		foreach ($message as $key => $value) {

			if ($value == $old_data[$key]) {
				unset($message[$key]);
				unset($_POST[$key]);
			}
		}

//======filter validations=======

		$valds = $this->users_model->validate;
// print_r($this->users_model->validate);

		foreach ($valds as $key => $vald) {
			// echo $vald['field'];

			// if($vald['field']=="email"){
			if (!array_key_exists($vald['field'], $message)) {

				// echo "haha";
				unset($this->users_model->validate[$key]);
			}

		}

		// print_r($this->users_model->validate);

		//======filter validations=======

		//============IMAGE HANDLER==========
		//if no_pic

		if ($this->input->post('no_pic')) {
			$message['image'] = "";
			$_POST['image'] = "";

			//delete image if any

		} else {
			//image upload form
			//incase of an image

			if (!empty($_FILES["userfile"]['name'])) {
//uploaded a file
				$did_upload = true;
//do both file validation + form validation

//file validation
				//image upload
				$config['upload_path'] = './uploads/users/images/';

				$config['allowed_types'] = 'jpeg|jpg|png';
				$extension = '.' . end(explode(".", $_FILES["userfile"]["name"]));
				$fileName = generate_token() . $extension;
				$config['file_name'] = $fileName;

				$this->load->library('upload');
				$this->upload->initialize($config);

				if ($this->upload->do_upload('userfile')) {
					$data = $this->upload->data();
					//upload success
					$upload_success = true;
				}
			}
		}
		//image upload form
		//============IMAGE HANDLER==========

		if ($upload_success) {
			//add image to the db
			$message['image'] = $fileName;
		}
		$response = $this->users_model->update($this->session->userdata('user_id'), $message);

		//2 cases
		//case 1  : only form was submitted and success
		//case 2  :  form + image upload were submitted and success
		if (($response && !$did_upload) || ($response && $upload_success)) {
			//flush cache

			//update session
			$this->users_model->update_session();
			// redirect('user/edit?ref=updated');
			redirect_home();

		} else {
			$data['error'] = validation_errors();

			// print_r($data);
			$this->load->view("user/edit", $data);

		}

	}

	public function login($redirection = false) {
		if (is_logged()) {
			redirect_home();
		}
		$data['page_title'] = "تسجيل الدخول";
		$data['redirection'] = $redirection;

		$this->load->view("user/login", $data);
	}
	public function logout() {

		$this->session->sess_destroy();

		redirect_home();
	}

	public function do_register() {

		$upload_success = false;
		$did_upload = false;
		$data_new = $_POST;
		$data_new = array(
			'email' => $_POST['email'],
			'password' => ($_POST['password']),
			'name' => $_POST['name'],
			'phone' => $_POST['phone'],
			);

		//============IMAGE HANDLER==========

		//image upload form
		//incase of an image

		if (!empty($_FILES["userfile"]['name'])) {
//uploaded a file
			$did_upload = true;
//do both file validation + form validation

//file validation
			//image upload
			$config['upload_path'] = './uploads/users/images/';

			$config['allowed_types'] = 'jpeg|jpg|png';
			$extension = '.' . end(explode(".", $_FILES["userfile"]["name"]));
			$fileName = generate_token() . $extension;
			$config['file_name'] = $fileName;

			$this->load->library('upload');
			$this->upload->initialize($config);

			if ($this->upload->do_upload('userfile')) {
				$data = $this->upload->data();
				//upload success
				$upload_success = true;
			}
		}

		//image upload form
		//============IMAGE HANDLER==========

		if ($upload_success) {
			//add image to the db
			$data_new['image'] = $fileName;
		}

		$response = $this->users_model->insert($data_new);

		//2 cases
		//case 1  : only form was submitted and success
		//case 2  :  form + image upload were submitted and success
		if (($response && !$did_upload) || ($response && $upload_success)) {
			//flush cache
			redirect('user/login?ref=updated');
		} else {
// echo "fail";
			$data['error'] = validation_errors();

			// print_r($data);
			$this->load->view("user/register", $data);

			// redirect('user/register?ref=update-error');
		}

	}

	/**
	 * method used to handle user's login request using his mail
	 *
	 * @access	public
	 */
	public function do_login($purpose = "direct") {
		// Check if the user has existing session to avoid duplication
		if (isset($this->session->userdata['user_id'])) {
			redirect_home();
		}

		// Get parameters
		$email = $this->input->post('email', TRUE);
		$password = $this->input->post('password', TRUE);
		//$remember = $this->input->post('remember_me', TRUE);

		// PHP validation check
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', null, 'required|valid_email');
		$this->form_validation->set_rules('password', null, 'required');
		if ($this->form_validation->run() == FALSE) {

			redirect('user/login?ref=error');
		}
		$password = md5($password);

		$response = $this->users_model->get_by(array('email' => $email, 'password' => $password, 'enabled' => 1));
		if ($response) {

			$this->sign_user_in($response);
			//if redirection
			if ($this->input->post('redirection', TRUE)) {
				redirect('classified_ads/add_mobawab');
			} else {
				redirect_home();
			}
		} else {
			$data['redirection'] = $this->input->post('redirection', TRUE);
			$data['error'] = "برجاء التأكد من البريد الالكتروني و كلمة المرور ";

			$this->load->view("user/login", $data);

			// redirect('user/login?ref=update-error',$data);
		}

	}

}