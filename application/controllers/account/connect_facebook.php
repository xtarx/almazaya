<?php
/*
 * Connect_facebook Controller
 */
class Connect_facebook extends CI_Controller {

	/**
	 * Constructor
	 */
	function __construct()
	{
		parent::__construct();

		// Load the necessary stuff...
		// $this->load->config('account/account');
		$this->load->helper(array('language', 'account/ssl', 'url'));
		$this->load->library(array('account/authentication', 'account/authorization', 'facebook'));
		$this->load->model(array('account/account_model', 'account/account_facebook_model'));
		$this->load->language(array('general', 'account/sign_in', 'account/account_linked', 'account/connect_third_party'));
	}

	function con()
	{
		$this->session->set_userdata('some_name', 'some_value');
		redirect('user');
	}

	function index()
	{
		// Enable SSL?
		// maintain_ssl($this->config->item("ssl_enabled"));

		$fb_data = $this->facebook->getUser();

		$fb_data_me = $this->facebook->api('/me');


		// Check if user is signed in on facebook
		if (isset($fb_data_me)) {
			

			$account=$fb_data_me;
				// print_r($account);
				// exit();

			// Check if user has connect facebook to a3m
			if ($user = $this->account_facebook_model->get_by_facebook_id($account['id']))
			{
				// Check if user is not signed in on a3m
				if ( ! is_logged())
				{
					// Run sign in routine
					$this->authentication->sign_in($user->account_id,$account,"facebook");
					// redirect('account/connect_create/facebook_sigin_in/'. $account['id']);
				}

				$user->account_id === $this->session->userdata('account_id') ? $this->session->set_flashdata('linked_error', sprintf(lang('linked_linked_with_this_account'), lang('connect_facebook'))) : $this->session->set_flashdata('linked_error', sprintf(lang('linked_linked_with_another_account'), lang('connect_facebook')));
				//already logged in and trying to link then direct to homepage
				//view to tell user the linked erros			
				// echo "Account is already linked assole";				
				redirect('');	
			}
			// The user has not connect facebook to a3m
			else
			{
				// Check if user is signed in on a3m
				if ( ! is_logged())
				{
							// exit();
					// Store user's facebook data in session
					$this->session->set_userdata('connect_create', array(array('provider' => 'facebook', 'provider_id' => $account['id']), array('fullname' =>  $account['name'], 'firstname' =>  $account['first_name'], 'lastname' =>  $account['last_name'], 'gender' =>  $account['gender'], //'dateofbirth' =>  $account['birthday'],	// not a required field, not all users have it set
						'picture' => 'http://graph.facebook.com/'. $account['id'].'/picture/?type=large')));
					
					// print_r($account);
					
					$this->load->model('facebook_session_bugs_model');
					$this->facebook_session_bugs_model->insert(array('provider' => 'facebook', 'provider_id' => $account['id'], 'fullname' =>  $account['name'], 'email' =>  $account['email'], 'firstname' =>  $account['first_name'], 'lastname' =>  $account['last_name'], 'gender' =>  $account['gender'],
						'picture' => 'http://graph.facebook.com/'. $account['id'].'/picture/?type=large'));					
					
					// Create a3m account
					redirect('account/connect_create/index/facebook/');
					
				}
				
			}
		}
		//else if not logged to fb, redirect to loginURL
            $login_url = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('account/connect_facebook'), 
                'scope' => array("email") // permissions here
            ));

		redirect($login_url);


	}

}

/* End of file connect_facebook.php */
/* Location: ./application/account/controllers/connect_facebook.php */
