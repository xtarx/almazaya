<?php
/*
 * Connect_create Controller
 */
class Connect_create extends CI_Controller {

	/**
	 * Constructor
	 */
	function __construct()
	{

		parent::__construct();


		$this->load->model('users_model');

		// Load the necessary stuff...
		$this->load->config('account/account');
		$this->load->helper(array('language', 'account/ssl', 'url'));
		$this->load->library(array('account/authentication', 'account/authorization', 'form_validation'));
		$this->load->model(array('account/account_model', 'account/account_details_model', 'account/account_facebook_model', 'account/account_twitter_model', 'account/account_openid_model'));
		$this->load->language(array('general', 'account/connect_third_party'));
	}

	
	public function con()
	{
		$this->load->view('account/facebook_additional_details_plus_email', NULL);

	}
	public function index($provider)
	{


		if(is_logged()){
			redirect_home();
		}

		$data['page_title']="تسجيل الحساب";



		switch ($provider) {
			case 'twitter':
			$this->load->view('account/twitter_additional_details', isset($data) ? $data : NULL);

			break;
			
			case 'facebook':

			// $data['connect_create'] = $this->session->userdata('connect_create');
			// $attributes=$data['connect_create'][1];
			// $this->load->model('facebook_session_bugs_model');
			// $account=$this->facebook_session_bugs_model->get_by(array('provider_id' =>$provider_id ));
				
			// print_r($account);
			// exit();	
			// echo "here";
			// if ( ! $account) redirect('');

			// $this->session->set_userdata('connect_create', array(array('provider' => 'facebook'), array( 'id' => $account['provider_id'],'name' =>  $account['fullname'],'email' =>  $account['email'])));

		if ( ! $this->session->userdata('connect_create')) redirect('');


		$data['connect_create'] = $this->session->userdata('connect_create');
		$attributes=$data['connect_create'][1];


			if(!empty($attributes['email'])){

				$this->load->view('account/facebook_additional_details', isset($data) ? $data : NULL);
			}else{
			//email is not provided
				$this->load->view('account/facebook_additional_details_plus_email', isset($data) ? $data : NULL);

			}
			break;
		}


	}

	public function do_twitter()
	{
   	// Redirect user to home if 'connect_create' session data doesn't exist
		if ( ! $this->session->userdata('connect_create')) redirect('');
		$data['page_title']="تسجيل الحساب";


		$data['connect_create'] = $this->session->userdata('connect_create');
		$attributes=$data['connect_create'][1];
		if (isset($attributes['screen_name'])) if (strlen($attributes['screen_name']) > 160) $attributes['screen_name'] = substr($attributes['screen_name'], 0, 160);

		$data_new= array(
			'email' => $this->input->post('email', TRUE),
			'name' => $attributes['screen_name'],
			'phone' => $this->input->post('phone', TRUE),
			'provider' => $data['connect_create'][0]['provider'],
			);

		$user_id = $this->users_model->create_account_for_provider($data_new);
		
		//validation

		if( $user_id)
		{

//if passed validation connect & login

		// Connect third party account to user				
			$this->account_twitter_model->insert($user_id, $data['connect_create'][0]['provider_id'], $data['connect_create'][0]['token'], $data['connect_create'][0]['secret']);
			
		// Run sign in routine
			$this->authentication->sign_in($user_id,$data['connect_create'],"twitter");
			
		}
		else
		{
			//else re-try 
			$data['error']= validation_errors(); 
			$this->load->view('account/twitter_additional_details', isset($data) ? $data : NULL);
		}


	}



	public function do_facebook()
	{
   	// Redirect user to home if 'connect_create' session data doesn't exist
		if ( ! $this->session->userdata('connect_create')) redirect('');
		$data['page_title']="تسجيل الحساب";


		$data['connect_create'] = $this->session->userdata('connect_create');
		$attributes=$data['connect_create'][1];

		// print_r($attributes);
		if (isset($attributes['fullname'])) if (strlen($attributes['fullname']) > 160) $attributes['fullname'] = substr($attributes['fullname'], 0, 160);

		$data_new= array(
			'name' => $attributes['fullname'],
			'phone' => $this->input->post('phone', TRUE),
			'provider' => $data['connect_create'][0]['provider'],
			'email' => $attributes['email'],

			);

		$user_id = $this->users_model->create_account_for_provider($data_new);
		
		//validation

		if( $user_id)
		{
		//if passed validation connect & login
		// Connect third party account to user				
			$this->account_facebook_model->insert($user_id,  $attributes['provider_id']);			
			// Run sign in routine
			$this->authentication->sign_in($user_id,$data['connect_create'],"facebook");
	
		}
		else
		{
			//else re-try 
			$data['error']= validation_errors(); 
			$this->load->view('account/facebook_additional_details', isset($data) ? $data : NULL);
		}


	}
/**
 * Facebook complete registeration this case using email
 * in additiomn to the phone
 */

public function do_facebook2()
{

		// print_r($this->session->all_userdata());
		// exit();
   	// Redirect user to home if 'connect_create' session data doesn't exist
	if ( ! $this->session->userdata('connect_create')) redirect('');
	$data['page_title']="تسجيل الحساب";


	$data['connect_create'] = $this->session->userdata('connect_create');
	$attributes=$data['connect_create'][1];
	if (isset($attributes['fullname'])) if (strlen($attributes['fullname']) > 160) $attributes['fullname'] = substr($attributes['fullname'], 0, 160);

	$data_new= array(

		'name' => $attributes['fullname'],
		'email' => $this->input->post('email', TRUE),
		'phone' => $this->input->post('phone', TRUE),
		'provider' => $data['connect_create'][0]['provider'],
		);

	$user_id = $this->users_model->create_account_for_provider($data_new);

		//validation

	if( $user_id)
	{
		//if passed validation connect & login
		// Connect third party account to user				
		$this->account_facebook_model->insert($user_id,  $attributes['provider_id']);			
			// Run sign in routine
		$this->authentication->sign_in($user_id,$data['connect_create'],"facebook");
	}
	else
	{
			//else re-try 
		$data['error']= validation_errors(); 
		$this->load->view('account/facebook_additional_details_plus_email', isset($data) ? $data : NULL);
	}


}




	/**
	 * Complete facebook's authentication process
	 *
	 * @access public
	 * @return void
	 */
	function fors()
	{
		print_r($this->session->all_userdata());	

		// Enable SSL?
		maintain_ssl($this->config->item("ssl_enabled"));

		// Redirect user to home if sign ups are disabled
		if ( ! ($this->config->item("sign_up_enabled"))) redirect('');

		// Redirect user to home if 'connect_create' session data doesn't exist
		if ( ! $this->session->userdata('connect_create')) redirect('');

		$data['connect_create'] = $this->session->userdata('connect_create');

		// Setup form validation
		$this->form_validation->set_error_delimiters('<span class="field_error">', '</span>');
		$this->form_validation->set_rules(array( array('field' => 'connect_create_email', 'label' => 'lang:connect_create_email', 'rules' => 'trim|required|valid_email|max_length[160]')));

		// Run form validation
		if ($this->form_validation->run())
		{
			
			// Check if email already exist
			if ($this->email_check($this->input->post('connect_create_email'), TRUE) === TRUE)
			{
				$data['connect_create_email_error'] = lang('connect_create_email_exist');
			}
			else
			{
				// Destroy 'connect_create' session data
				$this->session->unset_userdata('connect_create');

				// Create user

				// Add user details
				// $this->account_details_model->update($user_id, $data['connect_create'][1]);
				$attributes=$data['connect_create'][1];
				if (isset($attributes['fullname'])) if (strlen($attributes['fullname']) > 160) $attributes['fullname'] = substr($attributes['fullname'], 0, 160);

				$data_new= array(
					'email' => $this->input->post('connect_create_email', TRUE),
					'name' => $attributes['fullname'],
					'phone' => $this->input->post('connect_create_phone', TRUE),
					'provider' => $data['connect_create'][0]['provider'],
					);

				$user_id = $this->users_model->create_account_for_provider($data_new);

				// Connect third party account to user
				switch ($data['connect_create'][0]['provider'])
				{
					case 'facebook':
					$this->account_facebook_model->insert($user_id, $data['connect_create'][0]['provider_id']);
					break;
					case 'twitter':
					$this->account_twitter_model->insert($user_id, $data['connect_create'][0]['provider_id'], $data['connect_create'][0]['token'], $data['connect_create'][0]['secret']);
					break;
					case 'openid':
					$this->account_openid_model->insert($data['connect_create'][0]['provider_id'], $user_id);
					break;
				}

				// Run sign in routine
				//$this->authentication->sign_in($user_id);
			}
		}

		$this->load->view('account/twitter_additional_details', isset($data) ? $data : NULL);
	}

	/**
	 * Check if a username exist
	 *
	 * @access public
	 * @param string
	 * @return bool
	 */
	function username_check($username)
	{
		return $this->account_model->get_by_username($username) ? TRUE : FALSE;
	}

	/**
	 * Check if an email exist
	 *
	 * @access public
	 * @param string
	 * @return bool
	 */
	function email_check($email)
	{
		return $this->account_model->get_by_email($email) ? TRUE : FALSE;
	}

}

/* End of file connect_create.php */
/* Location: ./application/account/controllers/connect_create.php */
